; sum of all elements from 0x30 to 0x3D
00 00 001A ; r0 <- sum
00 10 001A ; r1 <- 2*i
00 40 001C ; r4 <- const 2
00 20 001E
33 24
11 22 0020 ; r2 <- x + 2*n (max address)

11 31 0020 ; r3 <- x + 2*i (current address)
25 32      ; cmp current and max address
81 00 0015 ; if equal then end
01 01 0020 ; sum += x[i]
21 14      ; i += 2
80 00 000B ; goto cycle
10 00 0018 ; r0 -> answer
99 00

0000 0000 ; answer
0000 0000 ; const 0
0000 0002 ; const 2
0000 0010 ; n

0000 0000 ; x[0]
0000 0001
0000 0002
0000 0003
0000 0004
0000 0005
0000 0006
0000 0007
0000 0008
0000 0009
0000 000A
0000 000B
0000 000C
0000 000D
0000 000E
0000 000F ; x[15]

