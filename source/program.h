#include <vector>

//DATATYPES
enum Model {
    model3, model2, modelStack, modelReg, modelMod
};

enum Execution_mode {
    executeModeAll, executeModeStep
};

enum Show_mode {
    showDefault, showNames, showDecimalUnsigned, showDecimalSigned
};

enum Cell_Error {
    noError, tooFewChar, tooManyChar, incorrectChar
};

class Digit16 {
private:
    unsigned char value;
    char letter;
    bool bits[4];

public:
    Digit16(int n);
    Digit16(char ch);
    Digit16();

    unsigned char get_value() const;
    char get_letter() const;
    bool operator[] (int i) const;
};

class Cell {
private:
    std::vector<Digit16> data;

public:
    Cell(int size, long long int value = 0);
    Cell();
    
    void set(long long int value);
    Digit16 get_digit(int i) const;
    void set_digit(int i, Digit16 digit);
    bool sign() const;
    bool bits(int i) const;
    int length() const;
    long long int value() const;
    long long int ivalue() const;
    int op_code() const;
    int op_value(int k) const;
    std::string show(int show_mode, int model = model3) const;
    int size() const;
    
    int create_from_string(std::string str);
};

class ArithmeticUnit {
public:
    ArithmeticUnit();
    void add(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF);
    void sub(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF);
    void mul(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF);
    void imul(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF);
    bool div(Cell a, Cell b, Cell &div, Cell &mod, bool &OF, bool &CF, bool &SF, bool &ZF);
    bool idiv(Cell a, Cell b, Cell &div, Cell &mod, bool &OF, bool &CF, bool &SF, bool &ZF);
};


class Computer {
protected:
    int cell_size;
    
public:
    std::vector<Cell> memory;
    int eip;
    bool OF, CF, ZF, SF;
    ArithmeticUnit arithmetic_unit;
    Cell command_register;
    
    int last_change_index;
    int last_change_second_index;
    bool is_finished;
    bool is_crashed;
    bool eip_changed;
    int iterations;
    
    Computer();

    Cell look (int i) const;
    Cell operator[] (int i);
    int size() const;
    bool load_from_file(std::string filepath, std::vector<std::wstring> &log);
    
    virtual bool execute_step(std::vector<std::wstring> &log) = 0;
    //virtual void show(int i, int show_mode) = 0;
    virtual ~Computer();
};


class ComputerModel3: public Computer {
public:
    ComputerModel3();
    bool execute_step(std::vector<std::wstring> &log);
    virtual ~ComputerModel3();
};

class ComputerModel2: public Computer {
public:
    ComputerModel2();
    bool execute_step(std::vector<std::wstring> &log);
    virtual ~ComputerModel2();
};

class ComputerModelStack: public Computer {
public:
    std::vector<Cell> stack;
    int esp;
    
    bool execute_step(std::vector<std::wstring> &log);
    Cell pop();
    Cell top() const;
    void push(Cell cell);
    ComputerModelStack();
    virtual ~ComputerModelStack();
};

class ComputerModelReg: public Computer {
public:
    std::vector<Cell> registers;
    ComputerModelReg();
    bool execute_step(std::vector<std::wstring> &log);
    virtual ~ComputerModelReg();
};

class ComputerModelMod: public Computer {
public:
    std::vector<Cell> registers;
    ComputerModelMod();
    bool execute_step(std::vector<std::wstring> &log);
    virtual ~ComputerModelMod();
};

//CONSTANTS
const int MAX_ITERATIONS = 1 << 16;
const int MEMORY_SIZE = 1 << 16;
const int STACK_SIZE = 20;
const int STACK_CELL_SIZE = 6;
const int REGISTER_SIZE = 16;
const int REGISTER_CELL_SIZE = 8;
const int MAX_FILE_NUMBER = 1 << 16;

//FUNCTIONS
void error(std::string str);

int cell_size(int model);

int from16(char ch);
char to16(int k);
std::string int_to_str16(int n, int max_size);

bool execute_all(Computer *computer, std::vector<std::wstring> &log);
bool execute_step(Computer *computer, std::vector<std::wstring> &log);

int substrs_in_str(std::wstring wstr, int substr_len);

std::string generate_new_filename(int model);
int file_format_to_model(std::string format);
std::string file_format_from_string(std::string filename);