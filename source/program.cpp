#include <string>
#include <iostream>
#include <fstream>

#include "program.h"

void error(std::string str)
{
    std::cout << "ERROR: " << str << std::endl;
    exit(0);
}

int from16(char ch) {
    if (ch >= 'A' && ch <= 'F') {
        return ch - 'A' + 10;
    } else if (ch >= '0' && ch <= '9') {
        return ch - '0';
    } else {
        error("Incorrect character for 16-point number system (from16): " + std::to_string(ch));
        return -1;
    }
}

char to16(int k) {
    if (k >= 0 && k <= 9) {
        return '0' + k;
    } else if (k >= 10 && k <= 16) {
        return 'A' + k - 10;
    } else {
        error("to16");
        return 'X';
    }
}

std::string int_to_str16(int n, int length) {
    int k = 0;
    std::string str;
    for (int i = 0; i < length + 2; ++i) {
        str.push_back('0');
    }
    while (k < length) {
        str[length + 1 - k] = to16(n % 16);
        n /= 16;
        ++k;
    }
    str[0] = '0';
    str[1] = 'x';
    return str;
}

std::string model_to_file_format(int model)
{
    switch (model) {
        case model3:
            return ".m3";
        case model2:
            return ".m2";
        case modelStack:
            return ".ms";
        case modelReg:
            return ".mr";
        case modelMod:
            return ".mm";
        default:
            break;
    }
    return "";
}

int file_format_to_model(std::string format)
{
    if (format == ".m3") {
        return model3;
    } else if (format == ".m2") {
        return model2;
    } else if (format == ".ms") {
        return modelStack;
    } else if (format == ".mr") {
        return modelReg;
    } else if (format == ".mm") {
        return modelMod;
    }
    
    return model3;
}

std::string file_format_from_string(std::string filename)
{
    int k = filename.size() - 1;
    while (k > 0) {
        if (filename[k] == '.') {
            break;
        }
        --k;
    }
    return filename.substr(k);
}

std::string generate_new_filename(int model)
{
    for (int i = 0; i < MAX_FILE_NUMBER; ++i) {
        std::string str = "programs/" + std::to_string(i) + model_to_file_format(model);
        std::ifstream file(str);
        if (!file.is_open()) {
            file.close();
            return str;
        }
    }
    
    error("Cannot generate new file name");
    return "programs/error.txt";
}


//struct Digit16
Digit16::Digit16(int n) {
    if (n < 0 || n > 15) {
        error("Digit16(int)");
    }

    value = n;
    letter = to16(n);

    for (int i = 3; i >= 0; --i) {
        bits[i] = n & 1;
        n >>= 1;
    }
}

Digit16::Digit16(char ch) {
    if ((ch < 'A' || ch > 'F') && (ch < '0' || ch > '9')) {
        error("Digit16(char)");
    }

    letter = ch;
    value = from16(ch);

    int n = value;
    for (int i = 3; i >= 0; --i) {
        bits[i] = n & 1;
        n >>= 1;
    }
}

Digit16::Digit16() {
    letter = '0';
    value = 0;

    for (int i = 3; i >= 0; --i) {
        bits[i] = 0;
    }
}

unsigned char Digit16::get_value() const
{
    return value;
}

char Digit16::get_letter() const
{
    return letter;
}

bool Digit16::operator[] (int i) const {
    if (i < 0 || i > 3) {
        error("Digit16[]");
    } else {
        return bits[i];
    }
}


Cell concat_cells(Cell c1, Cell c2)
{
    int len1 = c1.size();
    int len2 = c2.size();
    Cell cell(len1 + len2);
    
    for (int i = 0; i < len1; ++i) {
        cell.set_digit(i, c1.get_digit(i));
    }
    for (int i = 0; i < len2; ++i) {
        cell.set_digit(len1 + i, c2.get_digit(i));
    }
    return cell;
}

//struct Cell
/*Cell::Cell(int s, const std::string &str, int &error) {
    data = std::vector<Digit16>(s);
    if (str == "/") {
        for (int i = 0; i < s; ++i) {
            data[i] = Digit16(0);
        }
        return;
    }
    
    int k = 0;
    for (auto &elem: str) {
        if (elem == ';') {
            break;
        }
        if (!isspace(elem)) {
            if (k >= s) {
                error = tooManyChar;
            } else {
                if ((elem < 'A' || elem > 'F') && (elem < '0' || elem > '9')) {
                    error = incorrectChar;
                    data[k] = Digit16();
                } else {
                    data[k] = Digit16(elem);
                }
                ++k;
            }
        }
    }
    if (k != s) {
        error = tooFewChar;
        for (int i = k; i < s; ++i) {
            data[i] = Digit16(0);
        }
    }
}*/

void Cell::set_digit(int i, Digit16 digit)
{
    data[i] = digit;
}

Digit16 Cell::get_digit(int i) const
{
    return data[i];
}

Cell::Cell(int s, long long int value) {
    data = std::vector<Digit16>(s);
    
    this->set(value);
}

int Cell::create_from_string(std::string str) {
    if (str.size() < data.size()) {
        return tooFewChar;
    } else if (str.size() > data.size()) {
        return tooManyChar;
    }
    
    for (int i = 0; i < str.size(); ++i) {
        char ch = str[i];
        if ((ch < 'A' || ch > 'F') && (ch < '0' || ch > '9')) {
            return incorrectChar;
        } else {
            data[i] = Digit16(ch);
        }
    }
    
    return noError;
}

Cell::Cell() {
    data = std::vector<Digit16>{};
}

void Cell::set(long long int value) {
    value %= (1LL << (data.size() * 4));
    if (value < 0) {
        value = (1LL << (data.size() * 4)) + value;
    }

    for (int i = data.size() - 1; i >= 0; --i) {
        data[i] = Digit16(int(value % 16));
        value /= 16;
    }
}

bool Cell::sign() const {
    return data[0][0] == 1;
}

bool Cell::bits (int i) const {
    return data[i / 4][i % 4];
}

int Cell::length() const {
    for (int i = 0; i < data.size() * 4; ++i) {
        if (this->bits(i)) {
            return data.size() * 4 - i;
        }
    }
    return 0;
}

long long int Cell::value() const {
    long long int ans = 0;
    for (int i = 0; i < data.size(); ++i) {
        ans = 16 * ans + data[i].get_value();
    }
    return ans;
}

long long int Cell::ivalue() const {
    long long int ans = 0;
    for (int i = 0; i < data.size(); ++i) {
        ans = 16 * ans + data[i].get_value();
    }
    if (this->sign()) {
        ans -= 1LL << (data.size() * 4);
    }
    return ans;
}

int Cell::op_code() const {
    return 16 * data[0].get_value() + data[1].get_value();
}

int Cell::op_value(int k) const {
    int ans = 0;
    if (2 + 4 * k + 3 >= data.size()) {
        error("Cell(op_value)");
    }
    for (int i = 0; i < 4; ++i) {
        ans = 16 * ans + data[2 + 4 * k + i].get_value();
    }
    return ans;
}

int Cell::size() const
{
    return data.size();
}

std::string Cell::show(int show_mode, int model) const
{
    if (this->size() < 2) {
        return "";
    }
    
    if (show_mode == showDecimalUnsigned) {
        return std::to_string(this->value());
    }
    
    if (show_mode == showDecimalSigned) {
        return std::to_string(this->ivalue());
    }

    int start_from = 0;
    std::string op_name = "";
    if (show_mode == showNames) {
        int op_code = this->op_code();
        if (model == model3 || model == model2) {
            switch (op_code) {
                case 0x01:
                    op_name = "add"; break;
                case 0x02:
                    op_name = "sub"; break;
                case 0x03:
                    op_name = "imul"; break;
                case 0x13:
                    op_name = "mul"; break;
                case 0x04:
                    op_name = "idiv"; break;
                case 0x14:
                    op_name = "div"; break;
                case 0x05:
                    if (model == model2) {
                        op_name = "cmp";
                    } else {
                        op_name = "05";
                    }
                    break;
                case 0x80:
                    op_name = "jmp"; break;
                case 0x99:
                    op_name = "stop"; break;
                case 0x81:
                    op_name = "je"; break;
                case 0x82:
                    op_name = "jne"; break;
                case 0x83:
                    op_name = "jli"; break;
                case 0x93:
                    op_name = "jl"; break;
                case 0x84:
                    op_name = "jbei"; break;
                case 0x94:
                    op_name = "jbe"; break;
                case 0x85:
                    op_name = "jlei"; break;
                case 0x95:
                    op_name = "jle"; break;
                case 0x86:
                    op_name = "jbi"; break;
                case 0x96:
                    op_name = "jb"; break;
                case 0x00:
                    op_name = "mov"; break;
                default:
                    op_name.push_back(data[0].get_letter());
                    op_name.push_back(data[1].get_letter());
                    break;
            }
        } else if (model == modelStack) {
            switch (op_code) {
                case 0x01:
                    op_name = "add"; break;
                case 0x02:
                    op_name = "sub"; break;
                case 0x03:
                    op_name = "imul"; break;
                case 0x13:
                    op_name = "mul"; break;
                case 0x04:
                    op_name = "idiv"; break;
                case 0x14:
                    op_name = "div"; break;
                case 0x05:
                    op_name = "cmp"; break;
                case 0x80:
                    op_name = "jmp"; break;
                case 0x99:
                    op_name = "stop"; break;
                case 0x81:
                    op_name = "je"; break;
                case 0x82:
                    op_name = "jne"; break;
                case 0x83:
                    op_name = "jli"; break;
                case 0x93:
                    op_name = "jl"; break;
                case 0x84:
                    op_name = "jbei"; break;
                case 0x94:
                    op_name = "jbe"; break;
                case 0x85:
                    op_name = "jlei"; break;
                case 0x95:
                    op_name = "jle"; break;
                case 0x86:
                    op_name = "jbi"; break;
                case 0x96:
                    op_name = "jb"; break;
                case 0x5A:
                    op_name = "push"; break;
                case 0x5B:
                    op_name = "pop"; break;
                case 0x5C:
                    op_name = "copy"; break;
                case 0x5D:
                    op_name = "swp"; break;
                default:
                    op_name.push_back(data[0].get_letter());
                    op_name.push_back(data[1].get_letter());
                    break;
            }
        } else if (model == modelReg || model == modelMod) {
            switch (op_code) {
                case 0x01:
                    op_name = "add"; break;
                case 0x02:
                    op_name = "sub"; break;
                case 0x03:
                    op_name = "imul"; break;
                case 0x13:
                    op_name = "mul"; break;
                case 0x04:
                    op_name = "idiv"; break;
                case 0x14:
                    op_name = "div"; break;
                case 0x05:
                    op_name = "cmp"; break;
                case 0x21:
                    op_name = "addR"; break;
                case 0x22:
                    op_name = "subR"; break;
                case 0x23:
                    op_name = "imulR"; break;
                case 0x33:
                    op_name = "mulR"; break;
                case 0x24:
                    op_name = "idivR"; break;
                case 0x34:
                    op_name = "divR"; break;
                case 0x25:
                    op_name = "cmpR"; break;
                case 0x80:
                    op_name = "jmp"; break;
                case 0x99:
                    op_name = "stop"; break;
                case 0x81:
                    op_name = "je"; break;
                case 0x82:
                    op_name = "jne"; break;
                case 0x83:
                    op_name = "jli"; break;
                case 0x93:
                    op_name = "jl"; break;
                case 0x84:
                    op_name = "jbei"; break;
                case 0x94:
                    op_name = "jbe"; break;
                case 0x85:
                    op_name = "jlei"; break;
                case 0x95:
                    op_name = "jle"; break;
                case 0x86:
                    op_name = "jbi"; break;
                case 0x96:
                    op_name = "jb"; break;
                case 0x00:
                    op_name = "mov"; break;
                case 0x20:
                    op_name = "movR"; break;
                case 0x10:
                    op_name = "movRM"; break;
                case 0x11:
                    if (model == modelMod) {
                        op_name = "get";
                    } else {
                        op_name = "11";
                    }
                    break;
                default:
                    op_name.push_back(data[0].get_letter());
                    op_name.push_back(data[1].get_letter());
                    break;
            }
        }
    } else {
        op_name.push_back(data[0].get_letter());
        op_name.push_back(data[1].get_letter());
    }

    std::string str = op_name;
    
    if ((model == modelReg || model == modelMod) && data.size() == 8) {
        for (int i = 2; i < data.size(); ++i) {
            str.push_back(data[i].get_letter());
            if (i % 4 == 3) {
                str.push_back(' ');
            }
        }
    } else {
        str.push_back(' ');
        for (int i = 2; i < data.size(); ++i) {
            str.push_back(data[i].get_letter());
            if (i % 4 == 1) {
                str.push_back(' ');
            }
        }
    }
    return str;
}


//class ArithmeticUnit

ArithmeticUnit::ArithmeticUnit() {}

void ArithmeticUnit::add(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int max_val = (1LL << 40) - 1;
    long long int ans = a.value() + b.value();
    result.set(ans);
    OF = a.sign() && b.sign() && !result.sign() || !a.sign() && !b.sign() && result.sign();
    CF = ans > max_val;
    ZF = result.value() == 0;
    SF = result.sign();
}

void ArithmeticUnit::sub(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int max_val = (1LL << 40) - 1;
    long long int ans = a.value() - b.value();
    result.set(ans);
    OF = a.sign() && !b.sign() && !result.sign() || !a.sign() && b.sign() && result.sign();
    CF = ans < 0;
    ZF = result.value() == 0;
    SF = result.sign();
}

void ArithmeticUnit::mul(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int max_val = (1LL << 40) - 1;
    long long int ans = a.value() * b.value();

    result.set(ans);
}

void ArithmeticUnit::imul(Cell a, Cell b, Cell &result, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int max_val = (1LL << 40) - 1;
    long long int ans = a.ivalue() * b.ivalue();

    result.set(ans);
    //flag OF is incorrect now
}

bool ArithmeticUnit::div(Cell a, Cell b, Cell &div, Cell &mod, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int bb = b.value();

    if (bb == 0) {
        return false;
    }

    div.set(a.value() / bb);
    mod.set(a.value() % bb);

    return true;
}

bool ArithmeticUnit::idiv(Cell a, Cell b, Cell &div, Cell &mod, bool &OF, bool &CF, bool &SF, bool &ZF)
{
    long long int bb = b.ivalue();

    if (bb == 0) {
        return false;
    }

    div.set(a.ivalue() / bb);
    mod.set(a.ivalue() % bb);

    return true;
}


//class Computer

Computer::Computer() : OF(false), CF(false), SF(false), ZF(false), is_finished(false), is_crashed(false)
{
    cell_size = 0;
    memory = {};
    eip = 0;
    arithmetic_unit = ArithmeticUnit();
    command_register = Cell();
    last_change_index = -1;
    last_change_second_index = -1;
    iterations = 0;
    eip_changed = false;
}

Cell Computer::look (int i) const
{
    i %= MEMORY_SIZE;
    if (i >= this->size()) {
        return Cell(cell_size);
    }
    return memory[i];
}

Cell Computer::operator[] (int i)
{
    i %= MEMORY_SIZE;
    if (i >= this->size()) {
        for (int j = this->size() - 1; j < i; ++j) {
            memory.push_back(Cell(cell_size, 0));
        }
    }
    return memory[i];
}

int Computer::size() const
{
    return memory.size();
}

bool Computer::load_from_file(std::string filepath, std::vector<std::wstring> &log)
{
    std::ifstream file(filepath);
    if (!file.is_open()) {
        log.push_back(L"Невозможно открыть файл:");
        std::wstring wfilepath(filepath.begin(), filepath.end());
        log.push_back(wfilepath);
        return false;
    }
    
    std::string input_str;
    int prog_size = 0;
    int file_size = 0;
    int errors = 0;
    
    while (getline(file, input_str)) {
        ++file_size;
        if (input_str == "/") {
            memory.push_back(Cell(cell_size));
            ++prog_size;
            continue;
        }
        
        std::string str = "";
        Cell cell(cell_size);
        int k = 0;
        while (k < input_str.size()) {
            char ch = input_str[k];
            if (isspace(ch)) {
                ++k;
                continue;
            }
            
            if (ch != ';') {
                str += ch;
            }
            
            if (str.size() == cell_size || ch == ';' || k == input_str.size() - 1) {
                if (str.size() > 0) {
                    int error = cell.create_from_string(str);
                    //std::wstring cnt = std::to_wstring(cell_size);
                    
                    switch (error) {
                        case tooFewChar:
                            ++errors;
                            log.push_back(L"Строка " + std::to_wstring(file_size) + L": слишком мало символов.");
                            break;
                        case tooManyChar:
                            ++errors;
                            log.push_back(L"Строка " + std::to_wstring(file_size) + L": слишком много символов.");
                            break;
                        case incorrectChar:
                            ++errors;
                            log.push_back(L"Строка " + std::to_wstring(file_size) + L": некорректный символ.");
                            break;
                        default:
                            break;
                    }
                    
                    memory.push_back(cell);
                    ++prog_size;
                }
                
                str = "";
            }
            
            if (ch == ';') {
                break;
            }
            ++k;
        }
    }
    
    file.close();
    
    if (errors == 0) {
        log.push_back(L"Программа успешно загружена!");
        return true;
    } else {
        is_crashed = true;
        log.push_back(std::to_wstring(errors) + L" ошибок при загрузке программы!");
        return false;
    }
}

Computer::~Computer() {};


//class ComputerModel3

ComputerModel3::ComputerModel3() : Computer()
{
    cell_size = 14;
}

bool ComputerModel3::execute_step(std::vector<std::wstring> &log)
{
    if (memory[eip].size() != cell_size) {
        error("Incorrect cell size for model3");
    }
    
    int op_code = memory[eip].op_code();
    int op1 = memory[eip].op_value(0);
    int op2 = memory[eip].op_value(1);
    int op3 = memory[eip].op_value(2);
    Cell c1 = this->look(op1), c2 = this->look(op2);
    command_register = memory[eip];
    
    eip += 1;
    
    last_change_index = -1;
    last_change_second_index = -1;
    eip_changed = false;
    
    switch (op_code) {
        case 0x01:
            arithmetic_unit.add(c1, c2, memory[op3], OF, CF, SF, ZF);
            last_change_index = op3;
            break;
        case 0x02:
            arithmetic_unit.sub(c1, c2, memory[op3], OF, CF, SF, ZF);
            last_change_index = op3;
            break;
        case 0x03:
            arithmetic_unit.imul(c1, c2, memory[op3], OF, CF, SF, ZF);
            last_change_index = op3;
            break;
        case 0x13:
            arithmetic_unit.mul(c1, c2, memory[op3], OF, CF, SF, ZF);
            last_change_index = op3;
            break;
        case 0x04:
            arithmetic_unit.idiv(c1, c2, memory[op3], memory[(op3 + 1) % MEMORY_SIZE], OF, CF, SF, ZF);
            last_change_index = op3;
            last_change_second_index = (op3 + 1) % MEMORY_SIZE;
            break;
        case 0x14:
            arithmetic_unit.div(c1, c2, memory[op3], memory[(op3 + 1) % MEMORY_SIZE], OF, CF, SF, ZF);
            last_change_index = op3;
            last_change_second_index = (op3 + 1) % MEMORY_SIZE;
            break;
        case 0x80:
            eip = op3;
            eip_changed = true;
            break;
        case 0x81:
            if (c1.value() == c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x82:
            if (c1.value() != c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x83:
            if (c1.ivalue() < c2.ivalue()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x93:
            if (c1.value() < c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x84:
            if (c1.ivalue() >= c2.ivalue()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x94:
            if (c1.value() >= c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x85:
            if (c1.ivalue() <= c2.ivalue()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x95:
            if (c1.value() <= c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x86:
            if (c1.ivalue() > c2.ivalue()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x96:
            if (c1.value() > c2.value()) {
                eip = op3;
                eip_changed = true;
            }
            break;
        case 0x00:
            memory[op3] = c1;
            last_change_index = op3;
            break;
        case 0x99:
            log.push_back(L"Выполнение успешно завершилось!");
            is_finished = true;
            --eip;
            return true;
            break;
        default:
            std::string str16 = int_to_str16(op_code, 2);
            std::wstring wstr16(str16.begin(), str16.end());
            log.push_back(L"Неизвестный код операции: " + wstr16);
            return false;
            break;
    }
    
    if (eip >= this->size()) {
        (*this)[eip];
    }
    
    return true;
}

ComputerModel3::~ComputerModel3() {}


//class ComputerModel2

ComputerModel2::ComputerModel2() : Computer()
{
    cell_size = 10;
}

bool ComputerModel2::execute_step(std::vector<std::wstring> &log)
{
    if (memory[eip].size() != cell_size) {
        error("Incorrect cell size for model2");
    }
    
    int op_code = memory[eip].op_code();
    int op1 = memory[eip].op_value(0);
    int op2 = memory[eip].op_value(1);
    Cell c1 = this->look(op1), c2 = this->look(op2), sub_cell(cell_size);
    command_register = memory[eip];

    eip += 1;
    
    last_change_index = -1;
    last_change_second_index = -1;
    eip_changed = false;
    
    switch (op_code) {
        case 0x01:
            arithmetic_unit.add(c1, c2, memory[op1], OF, CF, SF, ZF);
            last_change_index = op1;
            break;
        case 0x02:
            arithmetic_unit.sub(c1, c2, memory[op1], OF, CF, SF, ZF);
            last_change_index = op1;
            break;
        case 0x05:
            arithmetic_unit.sub(c1, c2, sub_cell, OF, CF, SF, ZF);
            break;
        case 0x03:
            arithmetic_unit.imul(c1, c2, memory[op1], OF, CF, SF, ZF);
            last_change_index = op1;
            break;
        case 0x13:
            arithmetic_unit.mul(c1, c2, memory[op1], OF, CF, SF, ZF);
            last_change_index = op1;
            break;
        case 0x04:
            arithmetic_unit.idiv(c1, c2, memory[op1], memory[(op1 + 1) % MEMORY_SIZE], OF, CF, SF, ZF);
            last_change_index = op1;
            last_change_second_index = (op1 + 1) % MEMORY_SIZE;
            break;
        case 0x14:
            arithmetic_unit.div(c1, c2, memory[op1], memory[(op1 + 1) % MEMORY_SIZE], OF, CF, SF, ZF);
            last_change_index = op1;
            last_change_second_index = (op1 + 1) % MEMORY_SIZE;
            break;
        case 0x80:
            eip = op2;
            eip_changed = true;
            break;
        case 0x81:
            if (ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x82:
            if (!ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x83:
            if (OF != SF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x93:
            if (CF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x84:
            if (OF == SF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x94:
            if (!CF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x85:
            if (OF != SF || ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x95:
            if (CF || ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x86:
            if (OF == SF && !ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x96:
            if (!CF && !ZF) {
                eip = op2;
                eip_changed = true;
            }
            break;
        case 0x00:
            memory[op1] = c2;
            last_change_index = op1;
            break;
        case 0x99:
            log.push_back(L"Выполнение успешно завершилось!");
            is_finished = true;
            --eip;
            return true;
            break;
        default:
            std::string str16 = int_to_str16(op_code, 2);
            std::wstring wstr16(str16.begin(), str16.end());
            log.push_back(L"Неизвестный код операции: " + wstr16);
            return false;
            break;
    }
    
    if (eip >= this->size()) {
        (*this)[eip];
    }
    
    return true;
}

ComputerModel2::~ComputerModel2() {}


//class ComputerModelStack

ComputerModelStack::ComputerModelStack() : Computer()
{
    cell_size = 2;
    stack = std::vector<Cell>(STACK_SIZE);
    esp = -1;
}
    
bool ComputerModelStack::execute_step(std::vector<std::wstring> &log)
{
    if (memory[eip].size() != cell_size) {
        error("Incorrect cell size for modelS");
    }
    
    int op_code = memory[eip].op_code();
    Cell c1, c2, res(STACK_CELL_SIZE), mod(STACK_CELL_SIZE);

    int op_address = this->look(eip + 1).value() * 256 + this->look(eip + 2).value();
    int op_value = (this->look(op_address).value() * 256 + this->look(op_address + 1).value()) * 256 + this->look(op_address + 2).value();
    command_register = memory[eip];
    
    last_change_index = -1;
    last_change_second_index = -1;
    eip_changed = false;
    
    switch (op_code) {
        case 0x01:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.add(c1, c2, res, OF, CF, SF, ZF);
            this->push(res);
            ++eip;
            break;
        case 0x02:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.sub(c1, c2, res, OF, CF, SF, ZF);
            this->push(res);
            ++eip;
            break;
        case 0x05:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.sub(c1, c2, res, OF, CF, SF, ZF);
            ++eip;
            break;
        case 0x03:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.imul(c1, c2, res, OF, CF, SF, ZF);
            this->push(res);
            ++eip;
            break;
        case 0x13:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.mul(c1, c2, res, OF, CF, SF, ZF);
            this->push(res);
            ++eip;
            break;
        case 0x04:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.idiv(c1, c2, res, mod, OF, CF, SF, ZF);
            this->push(res);
            this->push(mod);
            ++eip;
            break;
        case 0x14:
            c1 = this->pop();
            c2 = this->pop();
            arithmetic_unit.div(c1, c2, res, mod, OF, CF, SF, ZF);
            this->push(res);
            this->push(mod);
            ++eip;
            break;
        
        case 0x5A:
            this->push(Cell(STACK_CELL_SIZE, op_value));
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            eip += 3;
            break;
        case 0x5B:
            res = this->pop();
            (*this)[op_address + 2];
            memory[op_address] = Cell(cell_size, res.value() / (1 << 16));
            memory[op_address + 1] = Cell(cell_size, (res.value() % (1 << 16)) / 256);
            memory[op_address + 2] = Cell(cell_size, res.value() % 256);
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            eip += 3;
            break;
        case 0x5C:
            c1 = this->top();
            this->push(c1);
            ++eip;
            break;
        case 0x5D:
            c1 = this->pop();
            c2 = this->pop();
            this->push(c1);
            this->push(c2);
            ++eip;
            break;
        case 0x80:
            eip = op_address;
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            eip_changed = true;
            break;
        case 0x81:
            if (ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x82:
            if (!ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x83:
            if (OF != SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x93:
            if (CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x84:
            if (OF == SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x94:
            if (!CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x85:
            if (OF != SF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x95:
            if (CF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x86:
            if (OF == SF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x96:
            if (!CF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 3;
            }
            command_register = Cell(6, command_register.value() * (1 << 16) + op_address);
            break;
        case 0x99:
            log.push_back(L"Выполнение успешно завершилось!");
            is_finished = true;
            return true;
            break;
        default:
            std::string str16 = int_to_str16(op_code, 2);
            std::wstring wstr16(str16.begin(), str16.end());
            log.push_back(L"Неизвестный код операции: " + wstr16);
            return false;
            break;
    }
    
    return true;
}

ComputerModelStack::~ComputerModelStack() {}

Cell ComputerModelStack::pop()
{
    if (esp <= -1) {
        error("pop() for empty stack");
        return Cell(STACK_CELL_SIZE);
    }
    return stack[esp--];
}

Cell ComputerModelStack::top() const
{
    if (esp <= -1) {
        error("top() for empty stack");
        return Cell(STACK_CELL_SIZE);
    }
    return stack[esp];
}

void ComputerModelStack::push(Cell cell)
{
    if (esp >= STACK_SIZE - 1) {
        error("push() for full stack");
        return;
    }
    stack[++esp] = cell;
}


//class ComputerModelReg

ComputerModelReg::ComputerModelReg() : Computer()
{
    cell_size = 4;
    registers = std::vector<Cell>(REGISTER_SIZE);
    for (int i = 0; i < registers.size(); ++i) {
        registers[i] = Cell(REGISTER_CELL_SIZE);
    }
}
    
bool ComputerModelReg::execute_step(std::vector<std::wstring> &log)
{
    if (memory[eip].size() != cell_size) {
        error("Incorrect cell size for modelS");
    }
    
    int op_code = memory[eip].op_code();
    int r1 = memory[eip].get_digit(2).get_value();
    int r2 = memory[eip].get_digit(3).get_value();
    //Cell c1, c2, res(STACK_CELL_SIZE), mod(STACK_CELL_SIZE);

    int op_address = this->look(eip + 1).value();
    Cell mem = concat_cells(this->look(op_address), this->look(op_address + 1));
    Cell res(REGISTER_CELL_SIZE);
    command_register = memory[eip];
    
    last_change_index = -1;
    last_change_second_index = -1;
    eip_changed = false;
    
    switch (op_code) {
        case 0x01:
            arithmetic_unit.add(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x02:
            arithmetic_unit.sub(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x05:
            arithmetic_unit.sub(registers[r1], mem, res, OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x03:
            arithmetic_unit.imul(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x13:
            arithmetic_unit.mul(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x04:
            arithmetic_unit.idiv(registers[r1], mem, registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x14:
            arithmetic_unit.div(registers[r1], mem, registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        
        case 0x21:
            arithmetic_unit.add(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x22:
            arithmetic_unit.sub(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x25:
            arithmetic_unit.sub(registers[r1], registers[r2], res, OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x23:
            arithmetic_unit.imul(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x33:
            arithmetic_unit.mul(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x24:
            arithmetic_unit.idiv(registers[r1], registers[r2], registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x34:
            arithmetic_unit.div(registers[r1], registers[r2], registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            eip += 1;
            break;
        
        case 0x80:
            eip = op_address;
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip_changed = true;
            break;
        case 0x81:
            if (ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x82:
            if (!ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x83:
            if (OF != SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x93:
            if (CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x84:
            if (OF == SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x94:
            if (!CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x85:
            if (OF != SF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x95:
            if (CF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x86:
            if (OF == SF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x96:
            if (!CF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x99:
            log.push_back(L"Выполнение успешно завершилось!");
            is_finished = true;
            return true;
            break;
        case 0x00:
            registers[r1] = mem;
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x20:
            registers[r1] = registers[r2];
            eip += 1;
            break;
        case 0x10:
            (*this)[op_address + 1];
            memory[op_address] = Cell(cell_size, registers[r1].value() / (1 << 16));
            memory[op_address + 1] = Cell(cell_size, registers[r1].value() % (1 << 16));
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        default:
            std::string str16 = int_to_str16(op_code, 2);
            std::wstring wstr16(str16.begin(), str16.end());
            log.push_back(L"Неизвестный код операции: " + wstr16);
            return false;
            break;
    }
    
    return true;
}


ComputerModelReg::~ComputerModelReg() {}


//class ComputerModelReg

ComputerModelMod::ComputerModelMod() : Computer()
{
    cell_size = 4;
    registers = std::vector<Cell>(REGISTER_SIZE);
    for (int i = 0; i < registers.size(); ++i) {
        registers[i] = Cell(REGISTER_CELL_SIZE);
    }
}

bool ComputerModelMod::execute_step(std::vector<std::wstring> &log)
{
    if (memory[eip].size() != cell_size) {
        error("Incorrect cell size for modelS");
    }
    
    int op_code = memory[eip].op_code();
    int r1 = memory[eip].get_digit(2).get_value();
    int r2 = memory[eip].get_digit(3).get_value();
    //Cell c1, c2, res(STACK_CELL_SIZE), mod(STACK_CELL_SIZE);

    int op_address = this->look(eip + 1).value();
    if (r2 != 0) {
        op_address += registers[r2].value();
    }
    Cell mem = concat_cells(this->look(op_address), this->look(op_address + 1));
    Cell res(REGISTER_CELL_SIZE);
    command_register = memory[eip];
    
    last_change_index = -1;
    last_change_second_index = -1;
    eip_changed = false;
    
    switch (op_code) {
        case 0x01:
            arithmetic_unit.add(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x02:
            arithmetic_unit.sub(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x05:
            arithmetic_unit.sub(registers[r1], mem, res, OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x03:
            arithmetic_unit.imul(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x13:
            arithmetic_unit.mul(registers[r1], mem, registers[r1], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x04:
            arithmetic_unit.idiv(registers[r1], mem, registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x14:
            arithmetic_unit.div(registers[r1], mem, registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        
        case 0x21:
            arithmetic_unit.add(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x22:
            arithmetic_unit.sub(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x25:
            arithmetic_unit.sub(registers[r1], registers[r2], res, OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x23:
            arithmetic_unit.imul(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x33:
            arithmetic_unit.mul(registers[r1], registers[r2], registers[r1], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x24:
            arithmetic_unit.idiv(registers[r1], registers[r2], registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            eip += 1;
            break;
        case 0x34:
            arithmetic_unit.div(registers[r1], registers[r2], registers[r1], registers[(r1 + 1) % REGISTER_SIZE], OF, CF, SF, ZF);
            eip += 1;
            break;
        
        case 0x80:
            eip = op_address;
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip_changed = true;
            break;
        case 0x81:
            if (ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x82:
            if (!ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x83:
            if (OF != SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x93:
            if (CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x84:
            if (OF == SF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x94:
            if (!CF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x85:
            if (OF != SF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x95:
            if (CF || ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x86:
            if (OF == SF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x96:
            if (!CF && !ZF) {
                eip = op_address;
                eip_changed = true;
            } else {
                eip += 2;
            }
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            break;
        case 0x99:
            log.push_back(L"Выполнение успешно завершилось!");
            is_finished = true;
            return true;
            break;
        case 0x00:
            registers[r1] = mem;
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x20:
            registers[r1] = registers[r2];
            eip += 1;
            break;
        case 0x10:
            (*this)[op_address + 1];
            memory[op_address] = Cell(cell_size, registers[r1].value() / (1 << 16));
            memory[op_address + 1] = Cell(cell_size, registers[r1].value() % (1 << 16));
            command_register = concat_cells(this->look(eip), this->look(eip + 1));
            eip += 2;
            break;
        case 0x11:
            registers[r1] = Cell(cell_size * 2, op_address);
            eip += 2;
            break;
        default:
            std::string str16 = int_to_str16(op_code, 2);
            std::wstring wstr16(str16.begin(), str16.end());
            log.push_back(L"Неизвестный код операции: " + wstr16);
            return false;
            break;
    }
    
    return true;
}

ComputerModelMod::~ComputerModelMod() {}


//execution of program

bool execute_all(Computer *computer, std::vector<std::wstring> &log)
{
    if (computer->is_crashed == true) {
        log.push_back(L"Программа некорректна!");
        return false;
    }
    if (computer->is_finished) {
        log.push_back(L"Программа уже завершилась!");
        return true;
    }
    
    computer->iterations = 0;
    while (1) {
        if (computer->iterations >= MAX_ITERATIONS) {
            log.push_back(L"Слишком много итераций (" + std::to_wstring(MAX_ITERATIONS) + L");");
            log.push_back(L"Вероятно, программа зациклилась.");
            log.push_back(L"Выполнение было прервано.");
            return false;
        }
        
        computer->execute_step(log);
        computer->iterations += 1;
        
        if (computer->is_finished) {
            //log.push_back(L"Программа завершилась!");
            return true;
        }
        if (computer->is_crashed == true) {
            log.push_back(L"Программа некорректна!");
            return false;
        }
    }
    return true;
}

bool execute_step(Computer *computer, std::vector<std::wstring> &log)
{
    if (computer->is_crashed == true) {
        log.push_back(L"Программа некорректна!");
        return false;
    }
    if (computer->is_finished) {
        log.push_back(L"Программа уже завершилась!");
        return true;
    }
    
    computer->execute_step(log);
    
    if (computer->is_finished) {
        //log.push_back(L"Программа завершилась!");
        return true;
    }
    if (computer->is_crashed == true) {
        log.push_back(L"Программа некорректна!");
        return false;
    }
    return true;
}

int substrs_in_str(std::wstring wstr, int substr_len)
{
    if (wstr == L"/") {
        return 1;
    }
    
    int cur_len = 0;
    for (int k = 0; k < wstr.size(); ++k) {
        if (wstr[k] == ';') {
            break;
        }
        if (!isspace(wstr[k])) {
            ++cur_len;
        }
    }
    
    return (cur_len + substr_len - 1) / substr_len;
}