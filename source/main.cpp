#include <SFML/Graphics.hpp>
#include <vector>
#include <list>
#include <fstream>
#include <clocale>

#include "graph.h"
#include "program.h"


int main()
{
    setlocale(LC_CTYPE, "rus");
    std::string app_name = "Emulator of computer model";
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name);
    bool fullscreen = false;
    
    ScrollField log_field(LOG_FIELD_X, LOG_FIELD_Y, LOG_FIELD_DX, LOG_FIELD_DY,
        LOG_FONT_SIZE, LOG_FONT_GAP, 0);
    ScrollField program_field(PROGRAM_FIELD_X, PROGRAM_FIELD_Y, PROGRAM_FIELD_DX, PROGRAM_FIELD_DY,
        PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);
    ScrollField notepad_field(NOTEPAD_FIELD_X, NOTEPAD_FIELD_Y, NOTEPAD_FIELD_DX, NOTEPAD_FIELD_DY,
        NOTEPAD_FONT_SIZE, NOTEPAD_FONT_GAP, 0);
    ScrollField stack_field(STACK_FIELD_X, STACK_FIELD_Y, STACK_FIELD_DX, STACK_FIELD_DY,
        PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);
    ScrollField register_field(REGISTER_FIELD_X, REGISTER_FIELD_Y, REGISTER_FIELD_DX, REGISTER_FIELD_DY,
        PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);

    GraphStorage pictures;
    
    if (!pictures.load_backs(BACKS_FILENAME))
    {
        error("Not found texture for back");
        exit(0);
    }

    if (!pictures.load_buttons(BUTTONS_FILENAME))
    {
        error("Not found texture for button");
    }
    
    if (!pictures.load_hints(HINTS_FILENAME))
    {
        error("Not found texture for hint");
    }
    
    if (!pictures.load_tutorials(TUTORIALS_FILENAME))
    {
        error("Not found texture for tutorial");
    }

    int selected_model_sprite;
    if (!pictures.add_texture("pictures/selectedModel.png", selected_model_sprite)) {
        error("Not found texture for selectedModel");
    }
    pictures[selected_model_sprite].setTextureRect(sf::IntRect(0, 0, 900, 100));

    int selected_switch_sprite;
    if (!pictures.add_texture("pictures/selectedSwitch.png", selected_switch_sprite)) {
        error("Not found texture for selectedSwitch");
    }
    
    int selected_tutorial_sprite;
    if (!pictures.add_texture("pictures/selectedTutorial.png", selected_tutorial_sprite)) {
        error("Not found texture for selectedTutorial");
    }
    
    std::vector<Button> buttons = define_buttons(pictures);
    
    sf::Font font;
    if (!font.loadFromFile(FONT_FILENAME)) {
        error("Not found font file");
    }
    
    int cur_room = mainMenu;

    int mouseX = 0, mouseY = 0;

    std::shared_ptr<Computer> computer;
    int show_mode = showDefault;

    std::vector<std::wstring> log{};
    
    std::string default_filepath = "programs/program.m3";
    std::string filepath = default_filepath;
    
    int model = model3;

    int instr_back_room = mainMenu;
    
    int tutorial = tutorialGeneral;

    sf::Clock idle_timer;
    sf::Clock cursor_timer;
    sf::Clock program_last_change_timer;
    bool cursor_visible = true;
    
    Notepad notepad;
    
    TextField user_filepath(USER_INPUT_X, USER_INPUT_Y, USER_INPUT_DX, USER_INPUT_DY,
        USER_FILEPATH_FONT_SIZE, FILEPATH_MAX_SIZE);
    user_filepath.set_string(filepath);
    
    bool creating_new_file = false;

    while (window.isOpen())
    {
        bool going_to_start = false;
        bool trying_to_save_file = false;
        int mouseX_old = mouseX;
        int mouseY_old = mouseY;
        mouseX = sf::Mouse::getPosition(window).x;
        mouseY = sf::Mouse::getPosition(window).y;
        if (mouseX != mouseX_old || mouseY != mouseY_old) {
            idle_timer.restart();
        }
        
        int log_old_size = log.size();

        //handling events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (event.type == sf::Event::MouseButtonPressed &&
                    event.mouseButton.button == sf::Mouse::Left) {
                idle_timer.restart();
                
                if (cur_room == inputRoom || cur_room == notepadSavingRoom) {
                    if (user_filepath.is_touched(mouseX, mouseY)) {
                        user_filepath.set_position(mouseX);
                    }
                }
                
                if (cur_room == notepadRoom && notepad_field.is_touched(mouseX, mouseY)) {
                    int row, col;
                    notepad_field.get_touched_position(mouseX, mouseY, row, col);
                    notepad.set_position(row, col - 8);
                }
                
                if (cur_room == gameRoom && program_field.is_button_touched(mouseX, mouseY)) {
                    program_field.is_mouse_scrolling = true;
                }
                if (cur_room == gameRoom && log_field.is_button_touched(mouseX, mouseY)) {
                    log_field.is_mouse_scrolling = true;
                }
                if (cur_room == gameRoom && stack_field.is_button_touched(mouseX, mouseY)) {
                    stack_field.is_mouse_scrolling = true;
                }
                if (cur_room == gameRoom && register_field.is_button_touched(mouseX, mouseY)) {
                    register_field.is_mouse_scrolling = true;
                }
                if (cur_room == notepadRoom && notepad_field.is_button_touched(mouseX, mouseY)) {
                    notepad_field.is_mouse_scrolling = true;
                }
                
                
                int selected_button = NO_BUTTON;
                for (auto &but: buttons) {
                    if (cur_room == but.room && but.is_touched(mouseX, mouseY)) {
                        selected_button = but.name;
                        break;
                    }
                }
                
                switch (selected_button) {
                    //mainMenu
                    case quitGame:
                        window.close();
                        break;
                    case toInstr:
                        instr_back_room = mainMenu;
                        cur_room = instrRoom;
                        break;
                    case startGame:
                        if (filepath == "") {
                            filepath = default_filepath;
                        }
                        user_filepath.set_string(filepath);
                        cur_room = inputRoom;
                        break;
                    
                    //inputRoom
                    case typingCancel:
                        cur_room = mainMenu;
                        break;
                    case typingClear:
                        user_filepath.set_string("");
                        break;
                    case typingOK:
                        filepath = user_filepath.content;
                        going_to_start = true;
                        break;
                    case chooseModel3:
                        model = model3;
                        creating_new_file = true;
                        break;
                    case chooseModel2:
                        model = model2;
                        creating_new_file = true;
                        break;
                    case chooseModelStack:
                        model = modelStack;
                        creating_new_file = true;
                        break;
                    case chooseModelReg:
                        model = modelReg;
                        creating_new_file = true;
                        break;
                    case chooseModelMod:
                        model = modelMod;
                        creating_new_file = true;
                        break;
                    case chooseModelBack:
                        cur_room = inputRoom;
                        break;
                    case inputNotepad:
                        filepath = user_filepath.content;
                        notepad = Notepad(filepath);
                        model = file_format_to_model(file_format_from_string(filepath));
                        if (!notepad.is_correct) {
                            ///TODO: file is closed
                        } else {
                            notepad_field.resize(notepad.size());
                            notepad_field.scroll_to_top();
                            cur_room = notepadRoom;
                        }
                        break;
                    case notepadNewFile:
                        cur_room = chooseModelRoom;
                        break;
                    
                    //gameRoom
                    case toMainMenu:
                        program_field.resize(0);
                        log_field.resize(0);
                        computer = {};
                        log.clear();
                        cur_room = mainMenu;
                        break;
                    case executeStep:
                        //log.push_back(L"Выполнение одного шага программы.");
                        execute_step(computer.get(), log);
                        program_last_change_timer.restart();
                        break;
                    case executeAll:
                        log.push_back(L"Выполнение программы до конца.");
                        execute_all(computer.get(), log);
                        program_last_change_timer.restart();
                        break;
                    case switchOpCodes:
                        if (show_mode == showNames) {
                            log.push_back(L"Показ содержимого в сырой форме.");
                            show_mode = showDefault;
                        } else {
                            log.push_back(L"Показ мнемонических имен команд.");
                            show_mode = showNames;
                        }
                        break;
                    case switchDecimalUnsigned:
                        if (show_mode == showDecimalUnsigned) {
                            log.push_back(L"Показ содержимого в сырой форме.");
                            show_mode = showDefault;
                        } else {
                            log.push_back(L"Показ содержимого как беззнаковые числа.");
                            show_mode = showDecimalUnsigned;
                        }
                        break;
                    case switchDecimalSigned:
                        if (show_mode == showDecimalSigned) {
                            log.push_back(L"Показ содержимого в сырой форме.");
                            show_mode = showDefault;
                        } else {
                            log.push_back(L"Показ содержимого как числа со знаком.");
                            show_mode = showDecimalSigned;
                        }
                        break;
                    
                    //notepadRoom
                    case notepadSave:
                        user_filepath.set_string(filepath);
                        cur_room = notepadSavingRoom;
                        break;
                    case notepadCopy:
                        notepad.copy();
                        break;
                    case notepadPaste:
                        notepad.paste();
                        break;
                    case notepadCut:
                        notepad.cut();
                        break;
                    case notepadAdd:
                        notepad.insert_line(notepad.eip + 1);
                        break;
                    case notepadHelp:
                        instr_back_room = notepadRoom;
                        cur_room = instrRoom;
                        break;
                    case notepadQuit:
                        notepad_field.resize(0);
                        notepad.clear();
                        user_filepath.set_string(filepath);
                        cur_room = inputRoom;
                        break;
                    
                    //notepadSavingRoom
                    case notepadFileConfirm:
                        filepath = user_filepath.content;
                        trying_to_save_file = true;
                        break;
                    case notepadFileReset:
                        user_filepath.set_string(filepath);
                        break;
                    case notepadFileClear:
                        user_filepath.set_string("");
                        break;
                    case notepadFileCancel:
                        cur_room = notepadRoom;
                        break;
                    
                    //instrRoom
                    case backInstr:
                        cur_room = instr_back_room;
                        break;
                    case instrGeneral:
                        tutorial = tutorialGeneral;
                        break;
                    case instrMemory:
                        tutorial = tutorialMemory;
                        break;
                    case instrNotepad:
                        tutorial = tutorialNotepad;
                        break;
                    case instrModel2:
                        tutorial = tutorialModel2;
                        break;
                    case instrModel3:
                        tutorial = tutorialModel3;
                        break;
                    case instrModelStack:
                        tutorial = tutorialModelStack;
                        break;
                    case instrModelReg:
                        tutorial = tutorialModelReg;
                        break;
                    case instrModelMod:
                        tutorial = tutorialModelMod;
                        break;
                    default:
                        break;
                }
            }
                
            if (event.type == sf::Event::MouseButtonReleased) {
                program_field.is_mouse_scrolling = false;
                log_field.is_mouse_scrolling = false;
                notepad_field.is_mouse_scrolling = false;
                stack_field.is_mouse_scrolling = false;
                register_field.is_mouse_scrolling = false;
            }

            if (event.type == sf::Event::MouseWheelScrolled &&
                    event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel) {
                idle_timer.restart();
                if (cur_room == gameRoom && program_field.is_touched(mouseX, mouseY)) {
                    program_field.scroll_with_wheel(event.mouseWheelScroll.delta);
                }
                if (cur_room == gameRoom && log_field.is_touched(mouseX, mouseY)) {
                    log_field.scroll_with_wheel(event.mouseWheelScroll.delta);
                }
                if (cur_room == notepadRoom && notepad_field.is_touched(mouseX, mouseY)) {
                    notepad_field.scroll_with_wheel(event.mouseWheelScroll.delta);
                }
                if (cur_room == gameRoom && stack_field.is_touched(mouseX, mouseY)) {
                    stack_field.scroll_with_wheel(event.mouseWheelScroll.delta);
                }
                if (cur_room == gameRoom && register_field.is_touched(mouseX, mouseY)) {
                    register_field.scroll_with_wheel(event.mouseWheelScroll.delta);
                }
            }
            
            if (event.type == sf::Event::KeyPressed) {
                idle_timer.restart();
                auto key = event.key.code;
                
                if (cur_room == notepadRoom) {
                    switch (key) {
                        case sf::Keyboard::Up:
                            notepad.up();
                            break;
                        case sf::Keyboard::Down:
                            notepad.down();
                            break;
                        case sf::Keyboard::Left:
                            notepad.left();
                            break;
                        case sf::Keyboard::Right:
                            notepad.right();
                            break;
                        case sf::Keyboard::End:
                            notepad.end();
                            break;
                        case sf::Keyboard::Home:
                            notepad.home();
                            break;
                        case sf::Keyboard::Enter:
                            notepad.insert_line(notepad.eip + 1);
                            break;
                        case sf::Keyboard::Delete:
                            notepad.del();
                            break;
                        default:
                            break;
                    }
                    
                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                        switch (key) {
                            case sf::Keyboard::V:
                                notepad.paste();
                                break;
                            case sf::Keyboard::C:
                                notepad.copy();
                                break;
                            case sf::Keyboard::X:
                                notepad.cut();
                                break;
                            case sf::Keyboard::Z:
                                notepad.undo();
                                break;
                            default:
                                break;
                        }
                    }
                }
                
                if (cur_room == notepadSavingRoom || cur_room == inputRoom) {
                    switch (key) {
                        case sf::Keyboard::Left:
                            user_filepath.left();
                            break;
                        case sf::Keyboard::Right:
                            user_filepath.right();
                            break;
                        case sf::Keyboard::End:
                            user_filepath.end();
                            break;
                        case sf::Keyboard::Home:
                            user_filepath.home();
                            break;
                        case sf::Keyboard::Delete:
                            user_filepath.del();
                            break;
                        case sf::Keyboard::Enter:
                            filepath = user_filepath.content;
                            if (cur_room == inputRoom) {
                                going_to_start = true;
                            } else {
                                trying_to_save_file = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            
            if (event.type == sf::Event::TextEntered) {
                if (cur_room == inputRoom || cur_room == notepadSavingRoom) {
                    char ch = static_cast<char>(event.text.unicode);
                    if (ch == '\b') {
                        user_filepath.backspace();
                    } else if (!isspace(ch) && !sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                        user_filepath.put_char(ch);
                    }
                }
                if (cur_room == notepadRoom) {
                    wchar_t wch = static_cast<wchar_t>(event.text.unicode);
                    if (wch == '\b') {
                        notepad.backspace();
                    } else if ((!isspace(wch) || wch == ' ') && !sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                        notepad.put_char(wch);
                    }
                }
                if (cur_room != inputRoom && cur_room != notepadSavingRoom && cur_room != notepadRoom) {
                    char ch = static_cast<char>(event.text.unicode);
                    if (ch == 'f') {
                        if (fullscreen) {
                            window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name);
                        } else {
                            window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), app_name, sf::Style::Fullscreen);
                        }
                        fullscreen = !fullscreen;
                    }
                }
            }
            
        }
        
        if (going_to_start) {
            log.clear();
            model = file_format_to_model(file_format_from_string(filepath));
            switch (model) {
                case model3:
                    computer = std::make_shared<ComputerModel3>();
                    break;
                case model2:
                    computer = std::make_shared<ComputerModel2>();
                    break;
                case modelStack:
                    computer = std::make_shared<ComputerModelStack>();
                    break;
                case modelReg:
                    computer = std::make_shared<ComputerModelReg>();
                    break;
                case modelMod:
                    computer = std::make_shared<ComputerModelMod>();
                    break;
                default:
                    break;
            }
            computer->load_from_file(filepath, log);
            
            if (model == modelStack) {
                program_field = ScrollField(PROGRAM_FIELD_X, PROGRAM_FIELD_Y, PROGRAM_FIELD_DX * 0.6, PROGRAM_FIELD_DY, PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);
            } else if (model == modelReg || model == modelMod) {
                program_field = ScrollField(PROGRAM_FIELD_X, PROGRAM_FIELD_Y, PROGRAM_FIELD_DX * 0.6 - 60, PROGRAM_FIELD_DY, PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);
            } else {
                program_field = ScrollField(PROGRAM_FIELD_X, PROGRAM_FIELD_Y, PROGRAM_FIELD_DX, PROGRAM_FIELD_DY, PROGRAM_FONT_SIZE, PROGRAM_FONT_GAP, 0);
            }
            
            program_field.resize(computer->size());
            program_field.scroll_to_top();
            log_field.resize(log.size());
            log_field.scroll_to_top();
            
            cur_room = gameRoom;
        }
        
        if (trying_to_save_file) {
            //TODO: show info about success or failure while saving
            if (notepad.save_to_file(filepath)) {
                cur_room = notepadRoom;
            }
        }
        
        if (creating_new_file) {
            creating_new_file = false;
            notepad = Notepad();
            notepad_field.resize(notepad.size());
            notepad_field.scroll_to_top();
            filepath = generate_new_filename(model);
            cur_room = notepadRoom;
        }
        
        //drawing
        window.clear(sf::Color::White);
        
        sf::Text text;
        text.setFont(font);
        text.setFillColor(APP_TEXT_COLOR);
        
        int draw_x, draw_y;
        int top_line, bottom_line;
        
        //drawing background
        window.draw(pictures.backs[cur_room]);
        
        //drawing tutorial slide
        if (cur_room == instrRoom) {
            window.draw(pictures.tutorials[tutorial]);
        }
        
        if (cur_room == gameRoom) {
            //drawing program
            program_field.resize(computer->size());
            program_field.draw_background(window);
            program_field.draw_left_margin(window, 7);
            
            text.setCharacterSize(PROGRAM_FONT_SIZE);
            top_line = program_field.top_visible_line();
            bottom_line = program_field.bottom_visible_line();
            
            if (computer->eip >= top_line && computer->eip <= bottom_line) {
                program_field.draw_selected_line(window, computer->eip);
            }
            
            draw_x = program_field.text_screen_x();
            for (int i = top_line; i <= bottom_line; ++i) {
                std::string str = (*computer)[i].show(show_mode, model);
                text.setString(int_to_str16(i, 4) + "  " + str);
                draw_y = program_field.text_screen_y(i);
                text.setPosition(draw_x, draw_y);
                window.draw(text);
                
                if (i == computer->last_change_index || i == computer->last_change_second_index) {
                    int alpha = std::max(0, std::min(255, 255 + PROGRAM_LAST_CHANGE_VISIBLE_MSEC - program_last_change_timer.getElapsedTime().asMilliseconds()));
                    text.setFillColor(sf::Color(255, 0, 0, alpha));
                    window.draw(text);
                    text.setFillColor(APP_TEXT_COLOR);
                }
            }
            program_field.draw_ui(window);
            
            //log
            log_field.resize(log.size());
            if (log.size() != log_old_size) {
                log_field.scroll_to_bottom();
            }
            log_field.draw_background(window);
            text.setCharacterSize(LOG_FONT_SIZE);
            top_line = log_field.top_visible_line();
            bottom_line = log_field.bottom_visible_line();
            
            draw_x = log_field.text_screen_x();
            for (int i = top_line; i <= bottom_line; ++i) {
                text.setString(log[i]);
                draw_y = log_field.text_screen_y(i);
                text.setPosition(draw_x, draw_y);
                window.draw(text);
            }
            log_field.draw_ui(window);
            
            if (ComputerModelStack *computer_stack = dynamic_cast<ComputerModelStack *>(computer.get())) {
                //drawing stack
                stack_field.resize(computer_stack->esp + 1);
                stack_field.draw_background(window);
                
                text.setCharacterSize(PROGRAM_FONT_SIZE);
                top_line = stack_field.top_visible_line();
                bottom_line = stack_field.bottom_visible_line();
                
                draw_x = stack_field.text_screen_x();
                for (int i = top_line; i <= bottom_line; ++i) {
                    std::string str = computer_stack->stack[i].show(show_mode, model);
                    text.setString(str);
                    draw_y = stack_field.text_screen_y(i);
                    text.setPosition(draw_x, draw_y);
                    window.draw(text);
                }
                stack_field.draw_ui(window);
            }
            
            if (ComputerModelReg *computer_reg = dynamic_cast<ComputerModelReg *>(computer.get())) {
                //drawing registers for register model
                register_field.resize(REGISTER_SIZE);
                register_field.draw_background(window);
                
                text.setCharacterSize(PROGRAM_FONT_SIZE);
                top_line = register_field.top_visible_line();
                bottom_line = register_field.bottom_visible_line();
                
                int local_show_mode = show_mode == showNames ? showDefault : show_mode;
                
                draw_x = register_field.text_screen_x();
                for (int i = top_line; i <= bottom_line; ++i) {
                    std::string str = computer_reg->registers[i].show(local_show_mode, model);
                    text.setString(int_to_str16(i, 1) + ": " + str);
                    draw_y = register_field.text_screen_y(i);
                    text.setPosition(draw_x, draw_y);
                    window.draw(text);
                }
                register_field.draw_ui(window);
            }
            
            if (ComputerModelMod *computer_reg = dynamic_cast<ComputerModelMod *>(computer.get())) {
                //drawing registers for modification model
                register_field.resize(REGISTER_SIZE);
                register_field.draw_background(window);
                
                text.setCharacterSize(PROGRAM_FONT_SIZE);
                top_line = register_field.top_visible_line();
                bottom_line = register_field.bottom_visible_line();
                
                int local_show_mode = show_mode == showNames ? showDefault : show_mode;
                
                draw_x = register_field.text_screen_x();
                for (int i = top_line; i <= bottom_line; ++i) {
                    std::string str = computer_reg->registers[i].show(local_show_mode, model);
                    text.setString(int_to_str16(i, 1) + ": " + str);
                    draw_y = register_field.text_screen_y(i);
                    text.setPosition(draw_x, draw_y);
                    window.draw(text);
                }
                register_field.draw_ui(window);
            }
        }

        //drawing notepad
        if (cur_room == notepadRoom) {
            notepad_field.resize(notepad.size());
            notepad_field.draw_background(window);
            notepad_field.draw_left_margin(window, 7);
            text.setCharacterSize(NOTEPAD_FONT_SIZE);
            top_line = notepad_field.top_visible_line();
            bottom_line = notepad_field.bottom_visible_line();
            
            if (notepad.eip >= top_line && notepad.eip <= bottom_line) {
                notepad_field.draw_selected_line(window, notepad.eip);
                if (cursor_visible) {
                    notepad_field.draw_cursor(window, notepad.eip, notepad.pos + 8);
                }
            }
            
            int str_num = 0;
            
            int cell_size = 0;
            if (model == model3) {
                cell_size = 14;
            } else if (model == model2) {
                cell_size = 10;
            } else if (model == modelStack) {
                cell_size = 2;
            } else if (model == modelReg) {
                cell_size = 4;
            } else if (model == modelMod) {
                cell_size = 4;
            }
            
            for (int i = 0; i < top_line; ++i) {
                if (notepad[i] != L"" && notepad[i][0] != ';') {
                    str_num += substrs_in_str(notepad[i], cell_size);
                }
            }
            
            draw_x = notepad_field.text_screen_x();
            for (int i = top_line; i <= bottom_line; ++i) {
                if (notepad[i] != L"" && notepad[i][0] != ';') {
                    std::string num16 = int_to_str16(str_num, 4);
                    std::wstring wnum16(num16.begin(), num16.end());
                    text.setString(wnum16 + L"  " + notepad[i]);
                    str_num += substrs_in_str(notepad[i], cell_size);
                } else {
                    text.setString(L"        " + notepad[i]);
                }
                
                draw_y = notepad_field.text_screen_y(i);
                text.setPosition(draw_x, draw_y);
                window.draw(text);
            }
            notepad_field.draw_ui(window);
        }

        //drawing last_command, flags
        if (cur_room == gameRoom) {
            //last_command
            text.setCharacterSize(32);
            text.setString(computer->command_register.show(show_mode, model));
            text.setPosition(820, 85);
            window.draw(text);

            //flags
            text.setCharacterSize(32);
            int flags_y = 260;
            int flags_x = 861;
            int flags_dx = 83;

            text.setString(std::to_string(int(computer->ZF)));
            text.setPosition(flags_x, flags_y);
            window.draw(text);
            text.setString(std::to_string(int(computer->SF)));
            text.setPosition(flags_x + flags_dx, flags_y);
            window.draw(text);
            text.setString(std::to_string(int(computer->OF)));
            text.setPosition(flags_x + 2 * flags_dx, flags_y);
            window.draw(text);
            text.setString(std::to_string(int(computer->CF)));
            text.setPosition(flags_x + 3 * flags_dx, flags_y);
            window.draw(text);

            text.setString(int_to_str16(computer->eip, 4));
            text.setPosition(870, 145);
            window.draw(text);
            if (computer->eip_changed) {
                int alpha = std::max(0, std::min(255, 255 + PROGRAM_LAST_CHANGE_VISIBLE_MSEC - program_last_change_timer.getElapsedTime().asMilliseconds()));
                text.setFillColor(sf::Color(255, 0, 0, alpha));
                window.draw(text);
                text.setFillColor(APP_TEXT_COLOR);
            }
        }

        //scrolling
        if (cur_room == gameRoom) {
            if (program_field.is_mouse_scrolling) {
                program_field.scroll_with_mouse(mouseY);
            }
        }
        if (cur_room == gameRoom) {
            if (log_field.is_mouse_scrolling) {
                log_field.scroll_with_mouse(mouseY);
            }
        }
        if (cur_room == gameRoom) {
            if (stack_field.is_mouse_scrolling) {
                stack_field.scroll_with_mouse(mouseY);
            }
        }
        if (cur_room == gameRoom) {
            if (register_field.is_mouse_scrolling) {
                register_field.scroll_with_mouse(mouseY);
            }
        }
        if (cur_room == notepadRoom) {
            if (notepad_field.is_mouse_scrolling) {
                notepad_field.scroll_with_mouse(mouseY);
            }
        }
        
        if (cur_room == inputRoom || cur_room == notepadSavingRoom) {
            user_filepath.draw(window, font, cursor_visible);
        }
            
        /*if (cur_room == chooseModelRoom) {
            int but_name;
            switch (model) {
                case model3:
                    but_name = chooseModel3;
                    break;
                case model2:
                    but_name = chooseModel2;
                    break;
                case modelStack:
                    but_name = chooseModelStack;
                    break;
                case modelReg:
                    but_name = chooseModelReg;
                    break;
                case modelMod:
                    but_name = chooseModelMod;
                    break;
                default:
                    but_name = chooseModel3;
                    break;
            }
            int x = WINDOW_WIDTH / 2 - 450;
            int y = buttons[but_name].y;
            pictures[selected_model_sprite].setPosition(x, y);
            window.draw(pictures[selected_model_sprite]);
        }*/

        if (cur_room == gameRoom) {
            int switch_x = buttons[switchOpCodes].x, switch_y;
            switch (show_mode) {
                case showNames:
                    switch_y = buttons[switchOpCodes].y;
                    break;
                case showDecimalUnsigned:
                    switch_y = buttons[switchDecimalUnsigned].y;
                    break;
                case showDecimalSigned:
                    switch_y = buttons[switchDecimalSigned].y;
                    break;
                default:
                    switch_y = -200;
                    break;
            }
            pictures[selected_switch_sprite].setPosition(switch_x - 3, switch_y - 3);
            window.draw(pictures[selected_switch_sprite]);
        }

        if (cur_room == instrRoom) {
            int tutorial_x = buttons[instrGeneral].x;
            int selected_button;
            switch (tutorial) {
                case tutorialGeneral:
                    selected_button = instrGeneral;
                    break;
                case tutorialMemory:
                    selected_button = instrMemory;
                    break;
                case tutorialNotepad:
                    selected_button = instrNotepad;
                    break;
                case tutorialModel2:
                    selected_button = instrModel2;
                    break;
                case tutorialModel3:
                    selected_button = instrModel3;
                    break;
                case tutorialModelStack:
                    selected_button = instrModelStack;
                    break;
                case tutorialModelReg:
                    selected_button = instrModelReg;
                    break;
                case tutorialModelMod:
                    selected_button = instrModelMod;
                    break;
                default:
                    selected_button = instrGeneral;
                    break;
            }
            int tutorial_y = buttons[selected_button].y;
            pictures[selected_tutorial_sprite].setPosition(tutorial_x - 3, tutorial_y - 3);
            pictures[selected_tutorial_sprite].setTextureRect(sf::IntRect(0, 0, buttons[selected_button].dx + 6, buttons[selected_button].dy + 6));
            window.draw(pictures[selected_tutorial_sprite]);
        }

        //drawing buttons
        for (int i = 0; i < buttons.size(); ++i) {
            if (buttons[i].room == cur_room) {
                window.draw(pictures.buttons[i]);
            }
        }

        //drawing hints
        if (idle_timer.getElapsedTime() >= sf::milliseconds(IDLE_TIME_FOR_HINT_MSEC)) {
            for (auto &but: buttons) {
                if (cur_room == but.room && but.is_touched(mouseX, mouseY)) {
                    pictures.hints[but.name].setPosition(mouseX, mouseY);
                    window.draw(pictures.hints[but.name]);
                    break;
                }
            }
        }

        if (cur_room == notepadRoom || cur_room == notepadSavingRoom || cur_room == inputRoom) {
            if (cursor_visible) {
                if (cursor_timer.getElapsedTime() > sf::milliseconds(CURSOR_TIME_VISIBLE_MSEC)) {
                    cursor_timer.restart();
                    cursor_visible = false;
                }
            } else {
                if (cursor_timer.getElapsedTime() > sf::milliseconds(CURSOR_TIME_INVISIBLE_MSEC)) {
                    cursor_timer.restart();
                    cursor_visible = true;
                }
            }
        }

        window.display();
    }

    return 0;
}
