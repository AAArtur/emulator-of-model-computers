#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <memory>
#include <fstream>
#include <locale>
#include <codecvt>
#include <string>

#ifndef _graph_h
#define _graph_h

//DATA STRUCTURES
enum Room {
    NO_ROOM = -1,
    mainMenu = 0,
    gameRoom,
    instrRoom,
    inputRoom,
    chooseModelRoom,
    notepadRoom,
    notepadSavingRoom,

    //number of rooms
    ROOM_MAX,
};

enum ButtonName {
    NO_BUTTON = -1,
    startGame = 0,
    toMainMenu,
    quitGame,
    toInstr,
    backInstr,

    executeStep,
    executeAll,
    switchOpCodes,
    switchDecimalUnsigned,
    switchDecimalSigned,

    typingOK,
    typingClear,
    typingCancel,
    chooseModel3,
    chooseModel2,
    chooseModelStack,
    chooseModelReg,
    chooseModelMod,
    chooseModelBack,

    notepadSave,
    notepadCopy,
    notepadPaste,
    notepadCut,
    notepadAdd,
    notepadHelp,
    notepadQuit,

    notepadQuitConfirm,
    notepadFileConfirm,
    notepadFileReset,
    notepadFileClear,
    notepadFileCancel,

    inputNotepad,
    notepadNewFile,

    instrGeneral,
    instrMemory,
    instrNotepad,
    instrModel2,
    instrModel3,
    instrModelStack,
    instrModelReg,
    instrModelMod,

    //number of buttons
    BUTTON_MAX
};

enum Tutorial {
    tutorialGeneral = 0,
    tutorialMemory,
    tutorialNotepad,
    tutorialModel2,
    tutorialModel3,
    tutorialModelStack,
    tutorialModelReg,
    tutorialModelMod,
    tutorialExample,

    //number of tutorials
    TUTORIAL_MAX
};

enum doAction {
    doNothing = 0,
    doSet,
    doPaste,
    doCut
};


class GraphStorage {
private:
    std::vector<std::shared_ptr<sf::Texture>> back_textures;
    std::vector<std::shared_ptr<sf::Texture>> button_textures;
    std::vector<std::shared_ptr<sf::Texture>> hint_textures;
    std::vector<std::shared_ptr<sf::Texture>> tutorial_textures;
    std::vector<std::shared_ptr<sf::Texture>> other_textures;

public:
    std::vector<sf::Sprite> backs;
    std::vector<sf::Sprite> buttons;
    std::vector<sf::Sprite> hints;
    std::vector<sf::Sprite> tutorials;
    std::vector<sf::Sprite> others;

    GraphStorage();

    bool load_backs(std::string filename);
    bool load_buttons(std::string filename);
    bool load_hints(std::string filename);
    bool load_tutorials(std::string filename);

    bool add_texture(std::string filename, int &number);
    
    sf::Sprite &operator[] (int i);
};


struct Button {
    int x, y;
    int dx, dy;
    int name;
    int room;

    Button();
    Button(int fx, int fy, int fdx, int fdy, int fname, int froom);

    bool is_touched(int mouse_x, int mouse_y) const;
};


struct NotepadAction {
    std::wstring str;
    int action;
    int index;

    NotepadAction(int act, int i, std::wstring s = L"");
    NotepadAction();
};


class NotepadUndoActions {
private:
    std::vector<NotepadAction> actions;
    int position;
    int cnt;
    int max_cnt;

public:
    int max_actions;

    NotepadUndoActions(int size = 1);

    void add_action(NotepadAction action);
    NotepadAction undo();
    NotepadAction redo();
};


class Notepad {
    bool going_ends;
    NotepadUndoActions undo_actions;
    int editing_line;
    std::wstring buffer;

public:
    std::vector<std::wstring> content;
    int eip;
    int pos;
    bool is_correct;

    Notepad();
    Notepad(std::string filename);

    void insert_line(int i);
    void remove_line(int i);
    void set_position(int eip, int pos);

    bool save_to_file(std::string filename) const;
    void clear();
    std::wstring &operator[] (int i);
    int size() const;

    void put_char(wchar_t wch);
    void up();
    void down();
    void right();
    void left();
    void end();
    void home();
    void del();
    void backspace();

    void copy();
    void paste();
    void cut();
    void undo();
    void redo();
    ///TODO: redo
    
    void set_string(int i, std::string str);
    void set_wstring(int i, std::wstring str);
};


class TextField {
private:
    int font_width;
    int text_screen_x() const;
    int text_screen_y() const;
    void get_touched_position(int mouse_x, int &pos) const;

public:
    std::string content;
    int pos;
    
    int x, y;
    int dx, dy;
    int font_size;
    int max_size;
    bool is_active;

    TextField(int x, int y, int dx, int dy, int font_size, int max_size);

    void put_char(char ch);
    void clear();
    void right();
    void left();
    void end();
    void home();
    void del();
    void backspace();
    
    bool is_touched(int mouse_x, int mouse_y) const;
    void set_string(std::string str);
    void set_position(int mouse_x);
    void draw(sf::RenderWindow &window, sf::Font &font, bool draw_cursor) const;
};


class ScrollField {
private:
    int button_x, button_y, button_dx, button_dy;
    int font_width;
    int pixels_per_scroll;
    int text_offset_x, text_offset_y;
    float scrolling, max_scrolling;
    float get_max_scrolling() const;
    void update_button();

public:
    int x, y;
    int dx, dy;
    int size;
    int font_size, font_gap;
    bool is_mouse_scrolling;

    ScrollField(int x, int y, int dx, int dy, int font_size, int font_gap, int size);
    void scroll_with_wheel(float delta);
    void scroll_with_mouse(int mouse_y);
    void scroll_to_top();
    void scroll_to_bottom();
    void resize(int new_size);
    
    bool is_touched(int mouse_x, int mouse_y) const;
    bool is_button_touched(int mouse_x, int mouse_y) const;
    
    int top_visible_line() const;
    int bottom_visible_line() const;
    int text_screen_x() const;
    int text_screen_y(int i) const;
    void get_touched_position(int mouse_x, int mouse_y, int &row, int &pos) const;
    
    void draw_background(sf::RenderWindow &window) const;
    void draw_ui(sf::RenderWindow &window) const;
    void draw_cursor(sf::RenderWindow &window, int row, int pos) const;
    void draw_selected_line(sf::RenderWindow &window, int row) const;
    void draw_left_margin(sf::RenderWindow &window, int margin_size) const;
};


//CONSTANTS
const std::string FONT_FILENAME = "RobotoMono-Regular.ttf";
const std::string BACKS_FILENAME = "pictures/backs/";
const std::string BUTTONS_FILENAME = "pictures/buttons/";
const std::string TUTORIALS_FILENAME = "pictures/tutorials/";
const std::string HINTS_FILENAME = "pictures/hints/";

const int PROGRAM_FONT_SIZE = 30;
const int PROGRAM_FONT_GAP = 2;
const int LOG_FONT_SIZE = 20;
const int LOG_FONT_GAP = 1;
const int NOTEPAD_FONT_SIZE = 25;
const int NOTEPAD_FONT_GAP = 1;
const int USER_FILEPATH_FONT_SIZE = 32;

const int NOTEPAD_LINE_MAX_SIZE = 80;
const int FILEPATH_MAX_SIZE = 32;

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;

const int PROGRAM_FIELD_X = 110;
const int PROGRAM_FIELD_Y = 20;
const int PROGRAM_FIELD_DX = 600;
const int PROGRAM_FIELD_DY = 680;

const int LOG_FIELD_X = 750;
const int LOG_FIELD_Y = 490;
const int LOG_FIELD_DX = 500;
const int LOG_FIELD_DY = 210;

const int NOTEPAD_FIELD_X = 110;
const int NOTEPAD_FIELD_Y = 20;
const int NOTEPAD_FIELD_DX = 1150;
const int NOTEPAD_FIELD_DY = 680;

const int STACK_FIELD_X = 510;
const int STACK_FIELD_Y = 20;
const int STACK_FIELD_DX = 200;
const int STACK_FIELD_DY = 680;

const int REGISTER_FIELD_X = 430;
const int REGISTER_FIELD_Y = 20;
const int REGISTER_FIELD_DX = 310;
const int REGISTER_FIELD_DY = 680;

const int USER_INPUT_X = 200;
const int USER_INPUT_Y = 220;
const int USER_INPUT_DX = 880;
const int USER_INPUT_DY = 50;

const int SCROLL_BUTTON_WIDTH = 20;
const int BIG_BUTTON_WIDTH = 300;
const int BIG_BUTTON_HEIGHT = 80;
const int MINI_BUTTON_WIDTH = 80;
const int MINI_BUTTON_HEIGHT = 80;

const int NOTEPAD_UNDO_MAX = 10;
const int NOTEPAD_STRLEN_MAX = 80;
const float FONT_WIDTH_COEF = 0.6;

const int IDLE_TIME_FOR_HINT_MSEC = 600;
const int CURSOR_TIME_VISIBLE_MSEC = 600;
const int CURSOR_TIME_INVISIBLE_MSEC = 100;
const int PROGRAM_LAST_CHANGE_VISIBLE_MSEC = 500;

const sf::Color APP_BACKGROUND_COLOR = sf::Color(64, 64, 64, 255);
const sf::Color APP_TEXT_COLOR = sf::Color(0, 0, 0, 255);
const sf::Color SCROLL_LINE_COLOR = sf::Color(200, 200, 200, 255);
const sf::Color SCROLL_BUTTON_COLOR = sf::Color(128, 128, 128, 255);
const sf::Color TEXT_BACKGROUND_COLOR = sf::Color(255, 255, 255, 255);
const sf::Color SELECTED_LINE_COLOR = sf::Color(255, 236, 129, 205);
//const sf::Color LEFT_MARGIN_COLOR = sf::Color(255, 246, 220, 255);
const sf::Color LEFT_MARGIN_COLOR = sf::Color(235, 235, 235, 255);

//FUNCTIONS
std::vector<Button> define_buttons(GraphStorage &textures);

#endif