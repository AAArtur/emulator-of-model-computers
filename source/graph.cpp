#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <memory>

#include "graph.h"


//FILENAMES

std::vector<std::string> back_filenames()
{
    std::vector<std::string> names(ROOM_MAX);
    names[mainMenu] = "mainMenu";
    names[gameRoom] = "gameRoom";
    names[instrRoom] = "instrRoom";
    names[inputRoom] = "inputRoom";
    names[chooseModelRoom] = "chooseModelRoom";
    names[notepadRoom] = "notepadRoom";
    names[notepadSavingRoom] = "notepadSavingRoom";

    return names;
}

std::vector<std::string> button_filenames()
{
    std::vector<std::string> names(BUTTON_MAX);
    names[startGame] = "startGame";
    names[toMainMenu] = "toMainMenu";
    names[quitGame] = "quitGame";
    names[toInstr] = "toInstr";
    names[backInstr] = "backInstr";
    names[executeStep] = "executeStep";
    names[executeAll] = "executeAll";
    names[switchOpCodes] = "switchOpCodes";
    names[switchDecimalUnsigned] = "switchDecimalUnsigned";
    names[switchDecimalSigned] = "switchDecimalSigned";
    names[typingOK] = "typingOK";
    names[typingClear] = "typingClear";
    names[typingCancel] = "typingCancel";
    names[chooseModel3] = "chooseModel3";
    names[chooseModel2] = "chooseModel2";
    names[chooseModelStack] = "chooseModelStack";
    names[chooseModelReg] = "chooseModelReg";
    names[chooseModelMod] = "chooseModelMod";
    names[chooseModelBack] = "chooseModelBack";
    names[notepadSave] = "notepadSave";
    names[notepadAdd] = "notepadAdd";
    names[notepadHelp] = "notepadHelp";
    names[notepadQuit] = "notepadQuit";
    names[notepadQuitConfirm] = "notepadQuitConfirm";
    names[notepadFileConfirm] = "notepadFileConfirm";
    names[notepadFileReset] = "notepadFileReset";
    names[notepadFileClear] = "notepadFileClear";
    names[notepadFileCancel] = "notepadFileCancel";
    names[inputNotepad] = "inputNotepad";
    names[notepadNewFile] = "notepadNewFile";
    names[instrGeneral] = "instrGeneral";
    names[instrMemory] = "instrMemory";
    names[instrNotepad] = "instrNotepad";
    names[instrModel2] = "instrModel2";
    names[instrModel3] = "instrModel3";
    names[instrModelStack] = "instrModelStack";
    names[instrModelReg] = "instrModelReg";
    names[instrModelMod] = "instrModelMod";
    names[notepadCopy] = "notepadCopy";
    names[notepadPaste] = "notepadPaste";
    names[notepadCut] = "notepadCut";

    return names;
}

std::vector<std::string> tutorial_filenames()
{
    std::vector<std::string> names(TUTORIAL_MAX);
    names[tutorialGeneral] = "tutorialGeneral";
    names[tutorialMemory] = "tutorialMemory";
    names[tutorialNotepad] = "tutorialNotepad";
    names[tutorialModel2] = "tutorialModel2";
    names[tutorialModel3] = "tutorialModel3";
    names[tutorialModelStack] = "tutorialModelStack";
    names[tutorialModelReg] = "tutorialModelReg";
    names[tutorialModelMod] = "tutorialModelMod";
    names[tutorialExample] = "tutorialExample";

    return names;
}


//LOCAL FUNCTIONS

bool load_pictures(std::vector<std::shared_ptr<sf::Texture>> &textures,
    std::vector<sf::Sprite> &sprites, std::vector<std::string> filenames, std::string filepath)
{
    textures.clear();
    textures.resize(filenames.size());
    sprites.clear();
    sprites.resize(filenames.size());

    for (int i = 0; i < filenames.size(); ++i) {
        sf::Texture tmp_texture;
        if (!tmp_texture.loadFromFile(filepath + filenames[i] + ".png")) {
            return false;
        }
        textures[i] = std::make_shared<sf::Texture>(tmp_texture);
        sprites[i] = sf::Sprite(*textures[i]);
    }

    return true;
}

bool if_point_hits_rectangle(int px, int py, int x, int y, int dx, int dy)
{
    return (px >= x && px <= x + dx && py >= y && py <= y + dy);
}


//struct GraphStorage
GraphStorage::GraphStorage() {}

bool GraphStorage::load_backs(std::string filepath)
{
    return load_pictures(back_textures, backs, back_filenames(), filepath);
}

bool GraphStorage::load_buttons(std::string filepath)
{
    return load_pictures(button_textures, buttons, button_filenames(), filepath);
}

bool GraphStorage::load_hints(std::string filepath)
{
    return load_pictures(hint_textures, hints, button_filenames(), filepath);
}

bool GraphStorage::load_tutorials(std::string filepath)
{
    return load_pictures(tutorial_textures, tutorials, tutorial_filenames(), filepath);
}

bool GraphStorage::add_texture(std::string filename, int &number)
{
    sf::Texture tmp_texture;
    sf::Sprite tmp_sprite;

    if (!tmp_texture.loadFromFile(filename)) {
        return false;
    }
    other_textures.push_back(std::make_shared<sf::Texture>(tmp_texture));

    tmp_sprite.setTexture(*other_textures[other_textures.size() - 1]);
    others.push_back(tmp_sprite);
    number = others.size() - 1;
    return true;
}

sf::Sprite &GraphStorage::operator[] (int i)
{
    return others[i];
}


//struct Button
Button::Button(int fx, int fy, int fdx, int fdy, int fname, int froom) :
    x(fx), y(fy), dx(fdx), dy(fdy), name(fname), room(froom) {}

Button::Button() :
    x(0), y(0), dx(0), dy(0), name(NO_BUTTON), room(NO_ROOM) {}

bool Button::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx, dy);
}


//struct NotepadAction
NotepadAction::NotepadAction(int act, int i, std::wstring s) : str(s), action(act), index(i) {}
NotepadAction::NotepadAction() : str(L""), action(doNothing), index(0) {}


//class NotepadUndoActions
NotepadUndoActions::NotepadUndoActions(int size)
{
    actions = std::vector<NotepadAction>(size);
    max_actions = size;
    position = 0;
    cnt = 0;
    max_cnt = 0;
}

void NotepadUndoActions::add_action(NotepadAction action)
{
    position = (position + 1) % max_actions;
    actions[position] = action;
    if (cnt < max_actions) {
        ++cnt;
    }
    max_cnt = cnt;
}
    
NotepadAction NotepadUndoActions::undo()
{
    if (cnt > 0) {
        NotepadAction action = actions[position];
        position = (position + max_actions - 1) % max_actions;
        --cnt;
        return action;
    }

    return NotepadAction();
}

NotepadAction NotepadUndoActions::redo()
{
    if (cnt < max_cnt) {
        position = (position + 1) % max_actions;
        NotepadAction action = actions[position];
        ++cnt;
        return action;
    }

    return NotepadAction();
}


//class Notepad
Notepad::Notepad()
{
    content = {L""};
    eip = 0;
    pos = 0;
    is_correct = true;
    going_ends = true;
    buffer = L"";
    undo_actions = NotepadUndoActions(NOTEPAD_UNDO_MAX);
    editing_line = -1;
}

Notepad::Notepad(std::string filename)
{
    content = {};
    eip = 0;
    pos = 0;
    
    std::wifstream file(filename);
    if (!file.is_open()) {
        is_correct = false;
        return;
    }
    std::wstring str;
    while (getline(file, str)) {
        content.push_back(str);
    }
    if (content.size() == 0) {
        content = {L""};
    }
    file.close();
    
    pos = content[eip].size();
    going_ends = true;
    buffer = L"";
    undo_actions = NotepadUndoActions(NOTEPAD_UNDO_MAX);
    editing_line = -1;
    is_correct = true;
}

void Notepad::put_char(wchar_t wch)
{
    if (content[eip].size() < NOTEPAD_STRLEN_MAX) {
        if (eip != editing_line) {
            editing_line = eip;
            undo_actions.add_action(NotepadAction(doSet, eip, content[eip]));
        }

        content[eip].insert(content[eip].begin() + pos, wch);
        ++pos;
    }
}

void Notepad::insert_line(int i)
{
    content.insert(content.begin() + i, L"");
    eip = i;
    pos = 0;
    going_ends = true;
    editing_line = -1;
}

void Notepad::remove_line(int i)
{
    if (content.size() <= 1) {
        return;
    }

    editing_line = -1;
    content.erase(content.begin() + i);
    eip = i;
    if (eip > 0) {
        --eip;
    }
    pos = content[eip].size();
    going_ends = true;
}

void Notepad::set_position(int new_eip, int new_pos)
{
    eip = std::max(0, std::min(new_eip, int(content.size()) - 1));
    pos = std::max(0, std::min(new_pos, int(content[eip].size())));
}

bool Notepad::save_to_file(std::string filename) const
{
    std::wofstream file(filename);
    if (!file.is_open()) {
        return false;
    }
    for (auto str: content) {
        file << str << '\n';
    }
    file.close();
    return true;
}

void Notepad::clear()
{
    content = {L""};
    eip = 0;
    pos = 0;
    going_ends = true;
    is_correct = true;
    buffer = L"";

    undo_actions = NotepadUndoActions(NOTEPAD_UNDO_MAX);
    editing_line = -1;
}

std::wstring &Notepad::operator[] (int i)
{
    return content[i];
}

int Notepad::size() const
{
    return content.size();
}

void Notepad::up()
{
    if (eip > 0) {
        --eip;
        if (going_ends) {
            pos = content[eip].size();
        } else {
            pos = pos > content[eip].size() ? content[eip].size() : pos;
        }
    }
}

void Notepad::down()
{
    if (eip < content.size() - 1) {
        ++eip;
        if (going_ends) {
            pos = content[eip].size();
        } else {
            pos = pos > content[eip].size() ? content[eip].size() : pos;
        }
    }
}

void Notepad::right()
{
    if (pos < content[eip].size()) {
        ++pos;
        if (pos == content[eip].size()) {
            going_ends = true;
        }
    }
}

void Notepad::left()
{
    if (pos > 0) {
        --pos;
        going_ends = false;
    }
}

void Notepad::end()
{
    pos = content[eip].size();
    going_ends = true;
}

void Notepad::home()
{
    pos = 0;
    going_ends = false;
}

void Notepad::del()
{
    if (pos < content[eip].size()) {
        if (eip != editing_line) {
            editing_line = eip;
            undo_actions.add_action(NotepadAction(doSet, eip, content[eip]));
        }
        content[eip].erase(content[eip].begin() + pos);
    }
}

void Notepad::backspace()
{
    if (content[eip].size() > 0) {
        if (pos > 0) {
            if (eip != editing_line) {
                editing_line = eip;
                undo_actions.add_action(NotepadAction(doSet, eip, content[eip]));
            }

            content[eip].erase(content[eip].begin() + pos - 1);
            --pos;
        }
    } else {
        if (eip > 0) {
            undo_actions.add_action(NotepadAction(doPaste, eip, content[eip]));
            this->remove_line(eip);
        }
    }
}

void Notepad::set_string(int i, std::string str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring wstr = converter.from_bytes(str);
    content[i] = wstr;
    pos = wstr.size();
    eip = i;
}

void Notepad::set_wstring(int i, std::wstring str)
{
    content[i] = str;
    pos = str.size();
    eip = i;
}

void Notepad::copy()
{
    buffer = content[eip];
}

void Notepad::paste()
{
    if (buffer != L"") {
        undo_actions.add_action(NotepadAction(doCut, eip + 1, L""));
        this->insert_line(eip + 1);
        this->set_wstring(eip, buffer);
    }
}

void Notepad::cut()
{
    buffer = content[eip];
    undo_actions.add_action(NotepadAction(doPaste, eip, content[eip]));
    this->remove_line(eip);
}

void Notepad::undo()
{
    NotepadAction action = undo_actions.undo();
    switch (action.action) {
        case doSet:
            this->set_wstring(action.index, action.str);
            editing_line = -1;
            break;
        case doPaste:
            this->insert_line(action.index);
            this->set_wstring(action.index, action.str);
            break;
        case doCut:
            this->remove_line(action.index);
            break;
        default:
            break;
    }
}

void Notepad::redo()
{
    return;
    NotepadAction action = undo_actions.redo();
    switch (action.action) {
        case doSet:
            this->set_wstring(action.index, action.str);
            editing_line = -1;
            break;
        case doPaste:
            this->insert_line(action.index);
            this->set_wstring(action.index, action.str);
            break;
        case doCut:
            this->remove_line(action.index);
            break;
        default:
            break;
    }
}


//class TextField
TextField::TextField(int fx, int fy, int fdx, int fdy, int ffont_size, int fmax_size) :
    x(fx), y(fy), dx(fdx), dy(fdy), font_size(ffont_size), max_size(fmax_size)
{
    content = "";
    pos = 0;
    is_active = false;
    font_width = font_size * FONT_WIDTH_COEF;
}

void TextField::put_char(char ch)
{
    content.insert(content.begin() + pos, ch);
    ++pos;
}

void TextField::clear()
{
    content = "";
    pos = 0;
}

void TextField::right()
{
    if (pos < content.size()) {
        ++pos;
    }
}

void TextField::left()
{
    if (pos > 0) {
        --pos;
    }
}

void TextField::end()
{
    pos = content.size();
}

void TextField::home()
{
    pos = 0;
}

void TextField::del()
{
    if (pos < content.size()) {
        content.erase(content.begin() + pos);
    }
}

void TextField::backspace()
{
    if (pos > 0) {
        content.erase(content.begin() + pos - 1);
        --pos;
    }
}

void TextField::set_string(std::string str)
{
    content = str;
    pos = str.size();
}

bool TextField::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx, dy);
}

void TextField::set_position(int mouse_x)
{
    this->get_touched_position(mouse_x, pos);
}

void TextField::draw(sf::RenderWindow &window, sf::Font &font, bool draw_cursor) const
{
    sf::RectangleShape back(sf::Vector2f(dx, dy));
    back.setFillColor(TEXT_BACKGROUND_COLOR);
    back.setPosition(x, y);
    window.draw(back);
    
    sf::Text text;
    text.setFont(font);
    text.setFillColor(APP_TEXT_COLOR);
    text.setCharacterSize(font_size);
    text.setString(content);
    text.setPosition(this->text_screen_x(), this->text_screen_y());
    window.draw(text);
    
    if (draw_cursor) {
        float cursor_dx = font_size * 0.08;
        float cursor_dy = std::min(float(dy), font_size * 1.2f);
        sf::RectangleShape cursor(sf::Vector2f(cursor_dx, cursor_dy));
        cursor.setFillColor(APP_TEXT_COLOR);
        int draw_x = this->text_screen_x() + pos * font_width;
        int draw_y = y + (dy - cursor_dy) / 2;
        cursor.setPosition(draw_x, draw_y);
        window.draw(cursor);
    }
}

int TextField::text_screen_x() const
{
    return x + dx / 2 - 1.0 * content.size() / 2 * font_width;
}

int TextField::text_screen_y() const
{
    return y + dy / 2 - font_size * 0.6;
}

void TextField::get_touched_position(int mouse_x, int &touched_pos) const
{
    int new_pos = (mouse_x - this->text_screen_x() + font_width * 0.5) / font_width;
    touched_pos = std::max(0, std::min(new_pos, int(content.size())));
}


//struct ScrollField
ScrollField::ScrollField(int fx, int fy, int fdx, int fdy, int ffont_size, int ffont_gap, int fsize) :
    x(fx), y(fy), dx(fdx), dy(fdy), font_size(ffont_size), font_gap(ffont_gap), size(fsize)
{
    scrolling = 0;
    pixels_per_scroll = font_size / 2;
    max_scrolling = this->get_max_scrolling();
    is_mouse_scrolling = false;
    font_width = font_size * FONT_WIDTH_COEF;
    
    button_dx = std::min(dx, SCROLL_BUTTON_WIDTH);
    button_x = x + dx - button_dx;
    this->update_button();
    text_offset_x = font_width * 0.5;
    text_offset_y = font_size * 0.5;
}

void ScrollField::scroll_with_wheel(float delta)
{
    scrolling -= delta;
    if (scrolling < 0) {
        scrolling = 0;
    }
    if (scrolling > max_scrolling) {
        scrolling = max_scrolling;
    }
    this->update_button();
}

void ScrollField::scroll_with_mouse(int mouse_y)
{
    int min_y = y;
    int max_y = y + dy - button_dy;
    if (max_y <= min_y) {
        return;
    }
    int my = std::min(max_y, std::max(min_y, mouse_y - button_dy / 2));
    scrolling = max_scrolling * (my - min_y) / (max_y - min_y);
    this->update_button();
}

void ScrollField::scroll_to_top()
{
    scrolling = 0;
    this->update_button();
}

void ScrollField::scroll_to_bottom()
{
    scrolling = max_scrolling;
    this->update_button();
}

void ScrollField::resize(int new_size)
{
    if (size != new_size) {
        size = new_size;
        max_scrolling = this->get_max_scrolling();
        scrolling = std::min(scrolling, max_scrolling);
        this->update_button();
    }
}

bool ScrollField::is_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, x, y, dx - button_dx, dy);
}

bool ScrollField::is_button_touched(int mouse_x, int mouse_y) const
{
    return if_point_hits_rectangle(mouse_x, mouse_y, button_x, button_y, button_dx, button_dy);
}

int ScrollField::top_visible_line() const
{
    return std::max(0, (int(scrolling) * pixels_per_scroll - text_offset_y) / (font_size + font_gap));
}

int ScrollField::bottom_visible_line() const
{
    return std::min(size - 1, this->top_visible_line() + dy / (font_size + font_gap) + 1);
}

int ScrollField::text_screen_x() const
{
    return x + text_offset_x;
}

int ScrollField::text_screen_y(int i) const
{
    return y + text_offset_y + i * (font_size + font_gap) - scrolling * pixels_per_scroll;
}

void ScrollField::get_touched_position(int mouse_x, int mouse_y, int &row, int &pos) const
{
    row = (mouse_y - y - text_offset_y + scrolling * pixels_per_scroll) / (font_size + font_gap);
    pos = (mouse_x - x - text_offset_x + font_width * 0.5) / font_width;
}

float ScrollField::get_max_scrolling() const
{
    return std::max(0, (size * (font_size + font_gap) - dy + dy / 5) / pixels_per_scroll);
}

void ScrollField::update_button()
{
    button_dy = std::min(dy, std::max(button_dx, int(dy / (dy + max_scrolling * pixels_per_scroll) * dy)));
    if (max_scrolling > 0) {
        button_y = y + (dy - button_dy) * (scrolling / max_scrolling);
    } else {
        button_y = y;
    }
}

void ScrollField::draw_background(sf::RenderWindow &window) const
{
    sf::RectangleShape back(sf::Vector2f(dx, dy));
    back.setFillColor(TEXT_BACKGROUND_COLOR);
    back.setPosition(x, y);
    window.draw(back);
}

void ScrollField::draw_ui(sf::RenderWindow &window) const
{
    sf::RectangleShape border(sf::Vector2f(dx, font_size));
    border.setFillColor(APP_BACKGROUND_COLOR);
    border.setPosition(x, y - font_size);
    window.draw(border);
    border.setPosition(x, y + dy);
    window.draw(border);
    
    sf::RectangleShape scroll_line(sf::Vector2f(button_dx, dy));
    scroll_line.setFillColor(SCROLL_LINE_COLOR);
    scroll_line.setPosition(x + dx - button_dx, y);
    window.draw(scroll_line);
    
    sf::RectangleShape scroll_button(sf::Vector2f(button_dx, button_dy));
    scroll_button.setFillColor(SCROLL_BUTTON_COLOR);
    scroll_button.setPosition(button_x, button_y);
    window.draw(scroll_button);
}

void ScrollField::draw_cursor(sf::RenderWindow &window, int row, int pos) const
{
    float cursor_dx = font_size * 0.08;
    float cursor_dy = font_size + 2 * font_gap;
    sf::RectangleShape cursor(sf::Vector2f(cursor_dx, cursor_dy));
    cursor.setFillColor(APP_TEXT_COLOR);
    int draw_x = this->text_screen_x() + pos * font_width;
    int draw_y = text_screen_y(row) + font_gap / 2;
    cursor.setPosition(draw_x, draw_y);
    window.draw(cursor);
}

void ScrollField::draw_selected_line(sf::RenderWindow &window, int row) const
{
    int draw_x = x;
    int draw_y = this->text_screen_y(row);
    sf::RectangleShape selected_line(sf::Vector2f(dx - button_dx, font_size + 3 * font_gap));
    selected_line.setFillColor(SELECTED_LINE_COLOR);
    selected_line.setPosition(draw_x, draw_y);
    window.draw(selected_line);
}

void ScrollField::draw_left_margin(sf::RenderWindow &window, int margin_size) const
{
    sf::RectangleShape margin(sf::Vector2f(font_width * margin_size + text_offset_x, dy));
    margin.setFillColor(LEFT_MARGIN_COLOR);
    margin.setPosition(x, y);
    window.draw(margin);
}


//FUNCTIONS

std::vector<Button> define_buttons(GraphStorage &pictures)
{
    std::vector<Button> buttons(BUTTON_MAX);
    buttons[quitGame]  = Button(40, 600, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, quitGame, mainMenu);
    buttons[toInstr]   = Button(40, 500, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, toInstr, mainMenu);
    buttons[startGame] = Button(40, 400, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, startGame, mainMenu);
    buttons[executeStep]           = Button(10, 120, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeStep, gameRoom);
    buttons[executeAll]            = Button(10, 220, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, executeAll, gameRoom);
    buttons[switchOpCodes]         = Button(10, 320, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, switchOpCodes, gameRoom);
    buttons[switchDecimalUnsigned] = Button(10, 420, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, switchDecimalUnsigned, gameRoom);
    buttons[switchDecimalSigned]   = Button(10, 520, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, switchDecimalSigned, gameRoom);
    buttons[toMainMenu]            = Button(10, 620, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, toMainMenu, gameRoom);
    buttons[typingOK]     = Button(860, 320, 120, 80, typingOK, inputRoom);
    buttons[typingClear]  = Button(560, 320, 80, 80, typingClear, inputRoom);
    buttons[typingCancel] = Button(300, 320, 200, 80, typingCancel, inputRoom);
    buttons[chooseModel3]     = Button(240, 80, 800, 100, chooseModel3, chooseModelRoom);
    buttons[chooseModel2]     = Button(240, 200, 800, 100, chooseModel2, chooseModelRoom);
    buttons[chooseModelStack] = Button(240, 320, 800, 100, chooseModelStack, chooseModelRoom);
    buttons[chooseModelReg]   = Button(240, 440, 800, 100, chooseModelReg, chooseModelRoom);
    buttons[chooseModelMod]   = Button(240, 560, 800, 100, chooseModelMod, chooseModelRoom);
    buttons[chooseModelBack]  = Button(40, 620, 200, 80, chooseModelBack, chooseModelRoom);
    buttons[notepadSave]   = Button(10,  20, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadSave, notepadRoom);
    buttons[notepadCopy]   = Button(10, 120, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadCopy, notepadRoom);
    buttons[notepadPaste]  = Button(10, 220, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadPaste, notepadRoom);
    buttons[notepadCut]    = Button(10, 320, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadCut, notepadRoom);
    buttons[notepadAdd]    = Button(10, 420, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadAdd, notepadRoom);
    buttons[notepadHelp]   = Button(10, 520, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadHelp, notepadRoom);
    buttons[notepadQuit]   = Button(10, 620, MINI_BUTTON_WIDTH, MINI_BUTTON_HEIGHT, notepadQuit, notepadRoom);
    buttons[notepadQuitConfirm] = Button(-400, 570, 200, 100, notepadQuitConfirm, notepadRoom);
    buttons[notepadFileConfirm] = Button(800, 320, 120, 80, notepadFileConfirm, notepadSavingRoom);
    buttons[notepadFileReset]   = Button(700, 320, 80, 80, notepadFileReset, notepadSavingRoom);
    buttons[notepadFileClear]   = Button(600, 320, 80, 80, notepadFileClear, notepadSavingRoom);
    buttons[notepadFileCancel]  = Button(360, 320, 200, 80, notepadFileCancel, notepadSavingRoom);
    buttons[inputNotepad] = Button(760, 320, 80, 80, inputNotepad, inputRoom);
    buttons[notepadNewFile]  = Button(660, 320, 80, 80, notepadNewFile, inputRoom);
    buttons[backInstr]    = Button(40, 620, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT, backInstr, instrRoom);
    buttons[instrGeneral] = Button(20,  20, 360, 80, instrGeneral, instrRoom);
    buttons[instrMemory]  = Button(20, 120, 360, 80, instrMemory, instrRoom);
    buttons[instrNotepad] = Button(20, 220, 360, 80, instrNotepad, instrRoom);
    buttons[instrModel2]     = Button(20, 320, 360, 50, instrModel2, instrRoom);
    buttons[instrModel3]     = Button(20, 380, 360, 50, instrModel3, instrRoom);
    buttons[instrModelStack] = Button(20, 440, 360, 50, instrModelStack, instrRoom);
    buttons[instrModelReg]   = Button(20, 500, 360, 50, instrModelReg, instrRoom);
    buttons[instrModelMod]   = Button(20, 560, 360, 50, instrModelMod, instrRoom);
    
    for (int i = 0; i < BUTTON_MAX; ++i) {
        pictures.buttons[i].setTextureRect(sf::IntRect(0, 0, buttons[i].dx, buttons[i].dy));
        pictures.buttons[i].setPosition(buttons[i].x, buttons[i].y);
    }
    
    return buttons;
}